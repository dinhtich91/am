$(document).ready(function () {
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });

    /*Intro*/
    var body_full = $("body").height();
    var body_quarter = body_full / 6;
    $(".intro-background").find(".intro-box").css("margin-top", body_quarter);
    $(".intro-background").find(".intro-box").css("margin-bottom", body_quarter);
    $(".intro-background").animateCss("zoomIn");
    $(".logo-intro-box").animateCss("rollIn");
    $(".intro-text-box").animateCss("slideInLeft");
    $(".img-max-width").hover(function () {
        $(this).animateCss("bounce");
    })
    /*End intro*/

    /*Home*/
    $(".home-slider").flexslider({
        animation: "slide",
        controlsContainer: $(".custom-controls-container-2"),
        customDirectionNav: $(".custom-navigation-2 a"),
    });
    $(".home-slider-2").flexslider({
        animation: "slide",
        controlsContainer: $(".custom-controls-container"),
        customDirectionNav: $(".custom-navigation a"),
        prevText: "",
        nextText: "",
    });
    $(".home-slider-3").flexslider({
        animation: "slide",
        itemWidth: 224,
        itemMargin: 5,
        prevText: "",
        nextText: "",
        maxItems: 4,
    });
    /*End home*/
    
    /*Product cong nghiep*/
    $(".thumb-nail-img").hover(function(){
        $(this).animateCss("pulse");
    })
    /*End product*/
})