﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.addTemplates('default',{
    imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates')+'templates/images/'),
    templates:[     
    {
        title:'Gói một hình lớn',
        image:'template4.gif',
        description:'Kích thước hình 490x255. Chú ý không nên thay đổi thông số mặt định',
        html:'<table width="490" style="width:490px !important; height:250px !important;" border="0" cellspacing="0" cellpadding="0" id="template4"><tr><td><img src="images/pic1.png" style="width:490px !important; height:250px !important;" alt="Firehouse"></td></tr></table>'
    },
    {
        title:'Gói gồm nhiều hình',
        image:'template5.gif',
        description:'Chú ý không nên thay đổi thông số mặt định',
        html:'<div style="width:488px; height:250px;"><div style="width:165px; height:250px;float:left; border-right: 3px solid white;"><img src="images/168x250.jpg" style="width:165px; height:250px;float:left; border-right: 3px solid white;"/></div><div style="width:318px; height:250px;float:left;"><div style="height:125px; clear:both;"><div style="width:200px; height:125px;float:left;"><img src="images/200x125.jpg"/></div><div style="width:115px; height:125px;float:left; border-left: 3px solid white;"><img src="images/118x125.jpg"/></div></div><div style="height:125px; clear:both;"><div style="width:94px; height:122px;float:left;border-top:3px solid white;"><img src="images/94x125.jpg" style="width:94px; height:122px;float:left;"/></div><div style="width:221px;height:125px;float:left;border-left:3px solid white;"><img src="images/224x125.jpg" style="width:221px;height:123px;float:left;border-top:3px solid white;"/></div></div></div></div>'
    },
    {
        title:'Image and Title',
        image:'template1.gif',
        description:'One main image with a title and text that surround the image.',
        html:'<h3><img style="margin-right: 10px" height="100" width="100" align="left"/>Type the title here</h3><p>Type the text here</p>'
    },{
        title:'Strange Template',
        image:'template2.gif',
        description:'A template that defines two colums, each one with a title, and some text.',
        html:'<table cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr><td style="width:50%"><h3>Title 1</h3></td><td></td><td style="width:50%"><h3>Title 2</h3></td></tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></table><p>More text goes here.</p>'
    },{
        title:'Text and Table',
        image:'template3.gif',
        description:'A title with some text and a table.',
        html:'<div style="width: 80%"><h3>Title goes here</h3><table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1"><caption style="border:solid 1px black"><strong>Table title</strong></caption></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table><p>Type the text here</p></div>'
    }]
});
