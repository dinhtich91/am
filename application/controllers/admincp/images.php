<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Images extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('admincp/user_model', 'user');
        $this->load->model('admincp/category_model', 'category');
        $this->load->model('admincp/images_model', 'images');
        $this->load->model(PATH_FOLDER_ADMIN.'/user_model', 'user');
        if ($this->user->checkUserPermission($this->session->userdata('idAdmin'),$this->Controller) == 0) {
            exit('No direct script access allowed');
        }
    }

    public function index() {
        if (!$this->session->userdata('idAdmin'))  redirect('admincp/login');
        $data['title_header']   = 'Hình ảnh';

        $data['headerCategory'] = $this->loadCategoryHeader();
        $data['list']     = $this->images->display();

        $this->load->view('admincp/view.header.php', $data);
        //$this->load->view('admincp/images/form.php');
        $this->load->view('admincp/images/list.php');
        $this->load->view('admincp/view.footer.php');
    }

    public function update($tag){
        if($this->input->post()) {
            $this->images->update($this->input->post(),$tag);
            redirect('admincp/images');
        } 
    }
    
    public function edit($tag){
       if (!$this->session->userdata('idAdmin'))  redirect('admincp/login');
        $data['title_header']   = 'Hình ảnh';

        $data['headerCategory'] = $this->loadCategoryHeader();
        
        $data['tagCategory']    = $tag;
        $data['list']           = $this->images->listImageByTag($tag);
        
        
        
        $this->load->view('admincp/view.header.php', $data);
        $this->load->view('admincp/images/form.php');        
        $this->load->view('admincp/view.footer.php');
    }
    public function loadCategoryHeader() {
        return $this->category->display(0, 0, FALSE);
    }

}

?>
