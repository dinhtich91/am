<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staticbaiviet extends CI_Controller {

    /**
     * Tên controller = tên thư mục(gồm form.php, list.php)
     */
    private $Controller = "staticbaiviet";
    private $task;
    private $arrayTitle = array(
        "intro" => "Intro",
        "gioi-thieu" => "Giới thiệu",
        "phuong-thuc-giao-hang" => "Phương thứ giao hàng",
        "phuong-thuc-thanh-toan" => "Phương thức thanh toán",
        "cauhinh-footer" => "Cấu hình footer"
    );

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('idAdmin'))
            redirect(PATH_FOLDER_ADMIN . '/login');
        $this->load->model(PATH_FOLDER_ADMIN . '/staticbaiviet_model', 'staticbaiviet');
        $this->load->model(PATH_FOLDER_ADMIN . '/user_model', 'user');
        $this->task = $this->task();

        //if ($this->user->checkUserPermission($this->session->userdata('idAdmin'), "staticbaiviet") == 0) {
            //exit('No direct script access allowed');
        //}
    }

    /**
     * Nạp link task thêm,sửa,xóa,danh sách,tình trạng ẩn hiện,submit form (Xóa chọn, sắp sếp nhanh)
     * Dạng folderadmin/controller/method
     */
    public function task() {
        $data['task_add'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/add";
        $data['task_edit'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/edit";
        $data['task_del'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/del";
        $data['task_list'] = PATH_FOLDER_ADMIN . "/" . $this->Controller;
        $data['task_status'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/status";
        $data['action_form'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/action";
        $data['page'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/p";
        $data['task_serach'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/search";
        return $data;
    }

    public function edit($type) {
        $data = $this->task;
        if ($this->input->post()) {
            $this->staticbaiviet->update($type);
            //$this->messages->add(MSG_EDIT_SUCCESS, 'success');
            redirect($data['task_edit'] . "/" . $type, "refresh");
        }

        $data['title_header'] = isset($this->arrayTitle[$type]) ? $this->arrayTitle[$type] : "Chưa có tiêu đề";
        $this->load->view(PATH_FOLDER_ADMIN . '/view.header.php', $data);
        /* #### */

        $data['detail'] = $this->staticbaiviet->getList($type);

        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/form', $data);
        $this->load->view(PATH_FOLDER_ADMIN . '/view.footer.php');
    }

}

?>
