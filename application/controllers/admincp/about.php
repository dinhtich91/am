<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');
class About extends CI_Controller {

    /**
     * Tên controller = tên thư mục(gồm form.php, list.php)
     */
    private $Controller = "about";    
    private $task;    
    public function __construct() {
        parent::__construct();    
        if(!$this->session->userdata('idAdmin')) redirect(PATH_FOLDER_ADMIN.'/login');
        $this->load->model(PATH_FOLDER_ADMIN.'/staticbaiviet_model', 'staticbaiviet');   
        $this->load->model(PATH_FOLDER_ADMIN.'/user_model', 'user');
        $this->task=$this->task();    
        if ($this->user->checkUserPermission($this->session->userdata('idAdmin'),$this->Controller) == 0) {
            exit('No direct script access allowed');
        }
    }

    /**
     * Nạp link task thêm,sửa,xóa,danh sách,tình trạng ẩn hiện,submit form (Xóa chọn, sắp sếp nhanh)
     * Dạng folderadmin/controller/method
     */
    public function task(){
        $data['task_add']      = PATH_FOLDER_ADMIN."/".$this->Controller."/add";
        $data['task_edit']     = PATH_FOLDER_ADMIN."/".$this->Controller."/edit";
        $data['task_del']      = PATH_FOLDER_ADMIN."/".$this->Controller."/del";
        $data['task_list']     = PATH_FOLDER_ADMIN."/".$this->Controller;
        $data['task_status']   = PATH_FOLDER_ADMIN."/".$this->Controller."/status";
        $data['action_form']   = PATH_FOLDER_ADMIN."/".$this->Controller."/action";
        $data['page']          = PATH_FOLDER_ADMIN."/".$this->Controller."/p";
        $data['task_serach']   = PATH_FOLDER_ADMIN."/".$this->Controller."/search";
        return $data;
    }       
    /**
     * Hươu Sophie
     * 
     */    
    public function index(){
        $data = $this->task;        
        if ($this->input->post()) {
            $this->staticbaiviet->aboutus();
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
            redirect($data['task_list']."/");
        }
        
        $data['title_header']   = "Hươu Sophie";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
        $data['detail']         = $this->staticbaiviet->getAboutus();       

        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/form',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
}

?>
