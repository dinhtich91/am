<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller {
    /*     * news
     * Tên controller = tên thư mục(gồm form.php, list.php)
     */

    private $Controller = "news";
    private $task;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('idAdmin'))
            redirect(PATH_FOLDER_ADMIN . '/login');
        $this->load->model(PATH_FOLDER_ADMIN . '/news_model', 'news');
        $this->load->model(PATH_FOLDER_ADMIN . '/news_category_model', 'news_category');
        $this->load->model(PATH_FOLDER_ADMIN . '/user_model', 'user');
        $this->task = $this->task();
        //if ($this->user->checkUserPermission($this->session->userdata('idAdmin'), $this->Controller) == 0) {
        // exit('No direct script access allowed');
        //}
    }

    /**
     * Nạp link task thêm,sửa,xóa,danh sách,tình trạng ẩn hiện,submit form (Xóa chọn, sắp sếp nhanh)
     * Dạng folderadmin/controller/method
     */
    public function task() {
        $calback = $this->session->userdata("CALLBACK");
        $data['task_list'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/p/" ;
        if ($calback) {
            $data['task_list'] = $calback;
        }
        $data['task_add'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/add/";
        $data['task_edit'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/edit/";
        $data['task_del'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/del/";

        $data['task_status'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/status/";
        $data['action_form'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/action/";
        $data['page'] = PATH_FOLDER_ADMIN . "/" . $this->Controller . "/p/";
        return $data;
    }

    public function index() {
        $this->p(0);
    }

    public function p($id_cat = 0, $page = 0) {
        $this->session->set_userdata(array("CALLBACK" => current_url()));
        $data = $this->task();
        $name = $this->news->getName("news_category", "id", $id_cat);
        if ($name != "#") {
            $data['title_header'] = "Tin tức - " . $name;
        } else {
            $data['title_header'] = "Tin tức";
        }
        $this->load->view(PATH_FOLDER_ADMIN . '/view.header.php', $data);

        $config['base_url'] = $data['page'];
        $config['total_rows'] = $this->news->total_rows($id_cat);
        $config['per_page'] = 100;
        $config['num_links'] = ADMIN_NUM_LINKS;
        $config['cur_page'] = $page;
        $this->pagination->initialize($config);
        $data['total_rows'] = $config['total_rows'];
        $data['list'] = $this->news->display($id_cat, $config['per_page'], $page);
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/list', $data);
        $this->load->view(PATH_FOLDER_ADMIN . '/view.footer.php');
    }

    /**
     * Addtion
     */
    public function add($id_cat = 0) {
        $data = $this->task($id_cat);
        if ($this->input->post()) {
            $this->news->add();
            $this->messages->add(MSG_ADD_SUCCESS, 'success');
            redirect($this->session->userdata("CALLBACK"));
        }

        $data['title_header'] = "Thêm mới - Tin tức";
        $this->load->view(PATH_FOLDER_ADMIN . '/view.header.php', $data);
        $data['orderingMax'] = $this->news->orderingMax();
        $data['id_cat_current'] = $id_cat;
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/form', $data);
        $this->load->view(PATH_FOLDER_ADMIN . '/view.footer.php');
    }

    public function edit($id_cat, $id) {
        $data = $this->task($id_cat);
        if ($this->input->post()) {
            $this->news->update($id);
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
            redirect($this->session->userdata("CALLBACK"));
        }
        $data['id_cat_current'] = $id_cat;
        $data['title_header'] = "Chỉnh sửa - Tin tức";
        $this->load->view(PATH_FOLDER_ADMIN . '/view.header.php', $data);
        $data['detail'] = $this->news->getList((int) $id);
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/form', $data);
        $this->load->view(PATH_FOLDER_ADMIN . '/view.footer.php');
    }

    /**
     * Chức năng : Xóa bằng href
     * @author : Huỳnh Văn Được - 20121123
     */
    public function del($id_cat, $id) {
        $data = $this->task($id_cat);
        $data = $this->task;
        $this->news->del($id);
        $this->messages->add(MSG_DEL_SUCCESS, 'success');
        redirect($data['task_list']);
    }

    /**
     * Chức năng : Ajax Hiện/Ẩn nhanh
     * @author : Huỳnh Văn Được - 20121123
     */
    public function status($id = 0, $status = 0, $field = 'status') {
        echo $this->news->status($id, $status, $field);
    }

    /**
     * Chức năng : Xóa nhiều & Sắp xếp nhanh
     * @author : Huỳnh Văn Được - 20121123
     */
    public function action() {
        $data = $this->task;
        if ($this->input->post("del")) {
            $this->news->del_all();
            $this->messages->add(MSG_DEL_SUCCESS, 'success');
        } else if ($this->input->post("ordering")) {
            $this->news->ordering_all();
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
        }
        redirect($data['task_list']);
    }

}

?>
