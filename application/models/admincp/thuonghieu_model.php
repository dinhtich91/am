<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Thuonghieu_model extends CI_Model { 
    public function __construct() {
        parent::__construct();
        $this->load->database();  
        $this->load->library("duocmaster");
    }
    /**
     * @todo: Hiển thị tất cả
     */
    public function display($num, $offset=0) {
        $this->db->select('*');
        $this->db->from(TBL_THUONGHIEU);
        $this->db->order_by('ordering', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    /**
     * @todo: Hiển thị tất cả
     */
    public function displayAll() {
        $this->db->select('*');
        $this->db->from(TBL_THUONGHIEU);
        $this->db->order_by('title', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($id) {
        $this->db->select('*');
        $this->db->from(TBL_THUONGHIEU);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result? $result[0] : array();
    }
    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add() {
        $params        = $this->input->post();
        $params['tag'] = $this->function->convertHTML($params['title']);
        $params['logo']= $this->duocmaster->uploadImage(PATH_LOGO,"logo");
        
        unset($params['tmp_logo']);
        $this->db->insert(TBL_THUONGHIEU, $params);
    }
    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update($id) {
        $params         = $this->input->post();        
        $params['tag']  = $this->function->convertHTML($params['title']);   
        $tmp_logo       = $params['tmp_logo'];
        $uploap_logo    = $this->duocmaster->uploadImage(PATH_LOGO,"logo");
        if($uploap_logo){
            $params['logo'] = $uploap_logo;
            @unlink($tmp_logo);
        }else{
            $params['logo'] = $tmp_logo;
        }
        
        unset($params['tmp_logo']);
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update(TBL_THUONGHIEU,$params);       
    }
    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del($id) {        
        return $this->function->del(TBL_THUONGHIEU,$id);
    }
    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id=0, $status=0,$field='status') {       
       return $this->function->status(TBL_THUONGHIEU,$id,$status,$field);
    }    
    /**
     * Lấy vị trí lớn nhất
     */
    public function orderingMax(){        
        return $this->function->orderingMax(TBL_THUONGHIEU);
    }
    /**
     * Chức năng xóa tất cả
     */
    public function del_all(){        
        $this->function->del_all(TBL_THUONGHIEU);
    }
    /**
     * Chức năng sắp xếp nhanh trong danh sách
     */
    public function ordering_all(){        
        $this->function->ordering_all(TBL_THUONGHIEU);
    }
    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows(){        
        $where = array();
        return $this->function->total_rows(TBL_THUONGHIEU,$where);
    }
    /**
     * Danh mục cha
     */
    public function parent($parent=0) {
        $this->db->select('*');
        $this->db->from(TBL_THUONGHIEU);
        $this->db->where(array('parent' => (int)$parent));
        $query = $this->db->get();
        if($query) return $query->result_array();
        else return NULL;
    }   
    
    /**
     * Menu đa cấp
     */
    public function dequycategory($cap=0,$gach="", $arr = NULL){
        $title  = "title";
        $result = $this->parent($cap);
        if(!$arr) $arr = array();//khoi tao 1 array co ten la arr  
        foreach($result as $row){
            $arr[] = array('id'=>$row['id'],"parent"=>$cap,$title=>$gach.$row[$title]); 
            $arr   = $this->dequycategory($row['id'],$gach."   -------  ",$arr);  
        }
        return $arr;
    }
    /**
     * Lấy tên danh mục cha
     */
    public function getNameParent($parent=0){
        $select  = "title";
        $where   = array('id'=>$parent);
        $result  = $this->function->getSelectTableWhere($select,TBL_THUONGHIEU,$where);
        return $result[$select]?$result[$select]:"#";
    }
   /**
    * @todo Lấy thương hiệu theo danh mục
    */
   public function getTrademarkByCate($strCate){
        $array_ = explode(",", $strCate);
        $this->db->select('*');
        $this->db->from(TBL_THUONGHIEU);
        $this->db->where_in("id",$array_);
        $this->db->order_by('title', 'asc');
        $query  = $this->db->get();
        return $query->result_array(); 
   }
}
?>
