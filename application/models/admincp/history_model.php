<?php

class History_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function display($num, $offset = 0) {
        $this->db->select('*');
        $this->db->from("history");
        $this->db->order_by('ordering', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Đếm số thumb của dự án
     */
    public function totalThumb($id) {
        $where = array("id" => $id);
        $result = $this->function->total_rows("history", $where);
        return $result;
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function displaySearch($filter_name = "") {
        $filter_name = $this->function->convertHTML($filter_name);
        $this->db->select('*');
        $this->db->from("history");
        if ($filter_name != "")
            $this->db->like('tag', $filter_name);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($id) {
        $this->db->select('*');
        $this->db->from("history");
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add() {
        $params = $this->input->post();
        $this->db->insert("history", $params);
        return TRUE;
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function insertMulipleImg($listIMG_DEFAULT, $listIMG_HOVER, $id) {
        $this->db->delete($this->TBL_SLIDE_IMAGE, array('id_history' => $id));
        foreach ($listIMG_DEFAULT as $i => $default_image) {
            $hover_image = $listIMG_HOVER[$i];
            $params = array(
                'id_history' => $id,
                'default_image' => $default_image,
                'hover_image' => $hover_image,
                'ordering' => $i,
            );
            $this->db->insert($this->TBL_SLIDE_IMAGE, $params);
        }
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update($id) {
        $params = $this->input->post();
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update("history", $params);
    }

    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del($id) {
        return $this->function->del("history", $id);
    }

    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id = 0, $status = 0, $field = 'status') {
        return $this->function->status("history", $id, $status, $field);
    }

    /**
     * Lấy vị trí lớn nhất
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function orderingMax() {
        return $this->function->orderingMax("history");
    }

    /**
     * Chức năng xóa tất cả
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del_all() {
        $this->function->del_all("history");
    }

    /**
     * Chức năng sắp xếp nhanh trong danh sách
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function ordering_all() {
        $this->function->ordering_all("history");
    }

    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows() {
        $where = array();
        return $this->function->total_rows("history", $where);
    }

    /**
     * Danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function parent($parent = 0) {
    $this->db->select('*');
    $this->db->from($this->TBL_SLIDE);
    $this->db->where(array('parent' => (int) $parent));
    $query = $this->db->get();
    if ($query)
        return $query->result_array();
    else
        return NULL;
}

/**
 * Menu đa cấp
 * @author : Huỳnh Văn Được
 * @copyright : Dpassion
 */
public function dequyMenu($cap = 0, $gach = "", $arr = NULL) {
    $title = "v_title";
    $result = $this->parent($cap);
    if (!$arr)
        $arr = array(); //khoi tao 1 array co ten la arr  
    foreach ($result as $row) {
        $arr[] = array('id' => $row['id'], "parent" => $cap, $title => $gach . $row[$title]);
        $arr = $this->dequyMenu($row['id'], $gach . "   -------  ", $arr);
    }
    return $arr;
}

/**
 * Lấy tên danh mục cha
 * @author : Huỳnh Văn Được
 * @copyright : Dpassion
 */
public function getNameParent($parent = 0) {
    $select = "v_title";
    $where = array('id' => $parent);
    $result = $this->function->getName($select, "history", $where);
    return $result[$select] ? $result[$select] : "#";
}

/**
 * Lấy tên danh mục cha
 * @author : Huỳnh Văn Được
 * @copyright : Dpassion
 */
public function getName($select, $table, $where, $value) {
    $where = array($where => $value);
    $result = $this->function->getSelectTableWhere($select, $table, $where);
    return $result? $result[$select] : "#";
}

}

?>
