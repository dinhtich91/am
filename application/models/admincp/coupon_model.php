<?php
class Coupon_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();        
    }

    /**
     * @todo: Hiển thị tất cả
     */
    public function display($num, $offset=0) {        
        $this->db->select('*');
        $this->db->from(TBL_COUPON);
        $this->db->order_by('status', 'asc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($id) {        
        $this->db->select('*');
        $this->db->from(TBL_COUPON);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add() {
        $data = $this->input->post();
        $char_number = $this->input->post('char_number');
        $price       = $this->input->post('price');
        $no          = $this->input->post('no');
        
        $array[]='0';
        $array[]='1';
        $array[]='2';
        $array[]='3';
        $array[]='4';
        $array[]='5';
        $array[]='6';
        $array[]='7';
        $array[]='8';
        $array[]='9';


        for($i=1;$i<=$no;$i++){
            $code="";
            for($j=0;$j<=$char_number;$j++){
                $char = rand(0, 9);
                $code.=$array[$char];
            }
            $data=array(
                'code'  =>$code,
                'price' =>$price,
                'status'=>0,
                'add_date' =>date("Y-m-d H:i:s")
            );
            $this->db->insert(TBL_COUPON, $data);            
        }
    }
    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del($id) {        
        return $this->function->del(TBL_COUPON,$id);
    }
    /**
     * @todo : Bật tắt tình trạng nhanh     */
    public function status($id=0, $status=0,$field='status') {       
       return $this->function->status(TBL_COUPON,$id,$status,$field);
    }    
    /**
     * Lấy vị trí lớn nhất
     */
    public function orderingMax(){        
        return $this->function->orderingMax(TBL_COUPON);
    }
    /**
     * Chức năng xóa tất cả
     */
    public function del_all(){        
        $this->function->del_all(TBL_COUPON);
    }
    /**
     * Chức năng sắp xếp nhanh trong danh sách
     */
    public function ordering_all(){        
        $this->function->ordering_all(TBL_COUPON);
    }
    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows(){        
        $where = array();
        return $this->function->total_rows(TBL_COUPON,$where);
    }
}
?>
