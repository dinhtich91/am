<?php
class Announcements_model extends CI_Model {

    private $TBL_MEMBER         = "members";
    private $TBL_MEMBER_COMPANY = "members_company";
    private $TBL_COMPANY        = "company";
    private $TBL_COMPANY_FACTORY= "company_factory";
    
    private $TBL_POST           = "post";
    private $TBL_ANNOUNCEMENTS  = "announcements";
    
    
    public function __construct() {
        parent::__construct();
        $this->load->database();        
    }

    /**
     * @todo: Hiển thị tất cả member theo type
     */
    public function display($num, $offset=0) {
        $table = $this->TBL_ANNOUNCEMENTS;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    /**
     * @todo :Lấy ID company theo member
     * @author :Huỳnh Văn Được <duoc.huynh@dpassion.com>
     */
    public function getIdCompanyByMember($id_member=0){
        $select  = "id_company";
        $table   = $this->TBL_MEMBER_COMPANY;        
        $where   = array('id_member'=>$id_member);
        $result  = $this->function->getSelectTableWhere($select,$table,$where);
        return $result[$select]?$result[$select]:"0";
    }
    
    /**
     * @todo Lấy các câu trả lời của chủ để
     * @author Huỳnh Văn Được 20130218 <duoc.huynh@dpassion.com>
     */
    public function getReplayByIdPost($id_post,$type=""){
        $select  = "id,text,status";
        $table   = $this->TBL_POST;        
        $where   = array('id_replay'=>$id_post,'type'=>$type);
        return $this->function->getMulSelectTableWhere($select,$table,$where);
    
    }


    /**
     * @todo Lấy tên công ty theo member
     * @author Huỳnh Văn  Được <duoc.huynh@dpassion.com>
     */
    public function getCompanyByMember($id_member=0){
        $id_company = $this->getIdCompanyByMember($id_member);        
        $table      = $this->TBL_COMPANY;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array("id"=>$id_company));
        $this->db->order_by('id', 'desc');        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    /**
     * @todo Get Factory by Company Id
     * @author Huỳnh Văn Được 20130201 <duoc.huynh@dpassion.com>
     */
    public function getFactoryByCompanyId($id_company=0){
        $select  = "*";
        $table   = $this->TBL_COMPANY_FACTORY;
        $where   = array('id_company'=>$id_company);
        return $this->function->getMulSelectTableWhere($select,$table,$where);      
    }
            
    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($id) {
        $table = $this->TBL_ANNOUNCEMENTS;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add() {
        $table              = $this->TBL_ANNOUNCEMENTS;
        $params             = $this->input->post();
        $params['content']  = nl2br($params['content']);
        $params['date']     = date("Y-m-d");
        $this->db->insert($table, $params);
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update($id) {
        $table              = $this->TBL_ANNOUNCEMENTS;
        $params             = $this->input->post();  
        $params['content']  = nl2br($params['content']);    
        $params['date']     = date("Y-m-d");
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update($table,$params);
    }
    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del($id) {
        $table = $this->TBL_ANNOUNCEMENTS;
        return $this->function->del($table,$id);
    }
    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id=0, $status=0,$field='status') {
       $table = $this->TBL_ANNOUNCEMENTS;
       return $this->function->status($table,$id,$status,$field);
    }    
    /**
     * Lấy vị trí lớn nhất
     */
    public function orderingMax(){
        $table = $this->TBL_MEMBER;
        return $this->function->orderingMax($table);
    }
    /**
     * Chức năng xóa tất cả
     */
    public function del_all(){
        $table = $this->TBL_ANNOUNCEMENTS;
        $this->function->del_all($table);
    }
    /**
     * Chức năng sắp xếp nhanh trong danh sách
     */
    public function ordering_all(){
        $table = $this->TBL_MEMBER;
        $this->function->ordering_all($table);
    }
    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows(){
        $table = $this->TBL_ANNOUNCEMENTS;
        return $this->function->total_rows($table);
    }
    /**
     * Danh mục cha
     */
    public function parent($parent=0) {
        $this->db->select('*');
        $this->db->from($this->TBL_MEMBER);
        $this->db->where(array('parent' => (int)$parent));
        $query = $this->db->get();
        if($query) return $query->result_array();
        else return NULL;
    }    
    
    /**
     * Menu đa cấp
     */
    public function dequycategory($cap=0,$gach="", $arr = NULL){
        $title  = "v_title";
        $result = $this->parent($cap);
        if(!$arr) $arr = array();//khoi tao 1 array co ten la arr  
        foreach($result as $row){
            $arr[] = array('id'=>$row['id'],"parent"=>$cap,$title=>$gach.$row[$title]); 
            $arr   = $this->dequycategory($row['id'],$gach."   -------  ",$arr);  
        }
        return $arr;
    }
    /**
     * Lấy tên danh mục cha
     */
    public function getNameParent($parent=0){
        $select  = "v_title";
        $table   = $this->TBL_MEMBER;
        $where   = array('id'=>$parent);
        $result  = $this->function->getSelectTableWhere($select,$table,$where);
        return $result[$select]?$result[$select]:"#";
    }

}

?>
