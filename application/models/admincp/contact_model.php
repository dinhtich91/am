<?php

class Contact_model extends CI_Model {

    private $TBL_CONTACT = "contact";
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @todo: Hiển thị tất cả
     */
    public function display($num, $offset=0) {
        $table = $this->TBL_CONTACT;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($id) {
        $table = $this->TBL_CONTACT;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add() {
        $table           = $this->TBL_CONTACT;
        $params          = $this->input->post();
        $params['v_tag'] = $this->function->convertHTML($params['v_title']);
        $this->db->insert($table, $params);
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update($id) {
        $table   = $this->TBL_CONTACT;
        $params  = $this->input->post();        
        $params['v_tag'] = $this->function->convertHTML($params['v_title']);
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update($table,$params);
    }
    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del($id) {
        $table = $this->TBL_CONTACT;
        return $this->function->del($table,$id);
    }
    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id=0, $status=0,$field='status') {
       $table = $this->TBL_CONTACT;
       return $this->function->status($table,$id,$status,$field);
    }    
    /**
     * Lấy vị trí lớn nhất
     */
    public function orderingMax(){
        $table = $this->TBL_CONTACT;
        return $this->function->orderingMax($table);
    }
    /**
     * Chức năng xóa tất cả
     */
    public function del_all(){
        $table = $this->TBL_CONTACT;
        $this->function->del_all($table);
    }
    /**
     * Chức năng sắp xếp nhanh trong danh sách
     */
    public function ordering_all(){
        $table = $this->TBL_CONTACT;
        $this->function->ordering_all($table);
    }
    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows(){
        $table = $this->TBL_CONTACT;
        $where = array();
        return $this->function->total_rows($table,$where);
    }
    /**
     * Danh mục cha
     */
    public function parent($parent=0) {
        $this->db->select('*');
        $this->db->from($this->TBL_CONTACT);
        $this->db->where(array('parent' => (int)$parent));
        $query = $this->db->get();
        if($query) return $query->result_array();
        else return NULL;
    }    
    
    /**
     * Menu đa cấp
     */
    public function dequycategory($cap=0,$gach="", $arr = NULL){
        $title  = "v_title";
        $result = $this->parent($cap);
        if(!$arr) $arr = array();//khoi tao 1 array co ten la arr  
        foreach($result as $row){
            $arr[] = array('id'=>$row['id'],"parent"=>$cap,$title=>$gach.$row[$title]); 
            $arr   = $this->dequycategory($row['id'],$gach."   -------  ",$arr);  
        }
        return $arr;
    }
    /**
     * Lấy tên danh mục cha
     */
    public function getNameParent($parent=0){
        $select  = "v_title";
        $table   = $this->TBL_CONTACT;
        $where   = array('id'=>$parent);
        $result  = $this->function->getSelectTableWhere($select,$table,$where);
        return $result[$select]?$result[$select]:"#";
    }

}

?>
