<?php
class Config_model extends CI_Model {   
    private $func;
    private $FOLDER_SILDE = "upload/images/";
    public function  __construct() {
        parent::__construct();
        $this->load->library('function');
        $this->load->database();
    }   
    public function get(){
        $this->db->select('*');
        $this->db->from('config');
        $query=$this->db->get();   
        $r    = $query->result_array();  
        return $r[0];
    }
    public function updateSMTP(){
        $this->load->library("duocmaster");
        $data = $this->input->post();

        $data['image_home'] = $data['temp_img'];
        $avartar = $this->duocmaster->uploadResize($this->FOLDER_SILDE,1628,405);
        if ($avartar) {
            $data['image_home'] = $avartar;
            @unlink($data['temp_img']);
        }
        unset($data['temp_img']);

        $this->db->update('config',$data);
        
    }

}

?>
