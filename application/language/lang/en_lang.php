<?php

$lang['trangchu'] = "Home";
$lang['lienhe'] = "Contact us";
$lang['gioithieu'] = "About us";
$lang['dandung'] = "residential";
$lang['congnghiep'] = "Industrialists";
$lang['tintuc'] = "News";
$lang['maunha'] = "form of house";
$lang['thuvien'] = "Gallery";

$lang['nhaamtinhviet'] = "Ngôi nhà ấm tình việt";

// Trang Lien he
$lang['hoten'] = "Your name";
$lang['diachi'] = "Address";
$lang['dienthoai'] = "Phone number";
$lang['email'] = "Email";
$lang['tinnhan'] = "Your message";
$lang['gui'] = "Send";

// Trang San Pham
$lang['thongtinsanpham'] = "Infomation";
$lang['thongso'] = "Specifications";
$lang['guiyeucau'] = "Send Contact";
$lang['giaban'] = "Price";
$lang['soluong'] = "Quantity";
$lang['tiente']  =" USD";

// Trang Tin tức
$lang['xemchitiet'] = "Read more";
$lang['tinlienquan'] = "Related News";
$lang['ungtuyen'] = "Apply";

$lang['dangcapnhat'] = "Updating ...";
$lang['lienhechungtoi'] = "Contact";
