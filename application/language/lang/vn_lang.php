<?php

$lang['trangchu'] = "Trang chủ";

$lang['lienhe'] = "Liên hệ";
$lang['gioithieu'] = "Giới thiệu";
$lang['dandung'] = "Nhà ấm dân dụng";
$lang['congnghiep'] = "Nhà ấm công nghiệp";
$lang['tintuc'] = "Tin tức";
$lang['maunha'] = "Mẫu nhà";
$lang['thuvien'] = "Thư viện";

$lang['nhaamtinhviet'] = "Ngôi nhà ấm tình việt";

// Trang Lien he
$lang['hoten'] = "Họ tên";
$lang['diachi'] = "Địa chỉ";
$lang['dienthoai'] = "Số điện thoại";
$lang['email'] = "Email";
$lang['tinnhan'] = "Nội dung";
$lang['gui'] = "Gửi";

// Trang San Pham
$lang['thongtinsanpham'] = "Thông tin sản phẩm";
$lang['thongso'] = "Thông số";
$lang['guiyeucau'] = "Gửi yêu cầu";
$lang['giaban'] = "Giá bán";
$lang['soluong'] = "Số lượng";
$lang['tiente']  =" đ";

// Trang Tin tức
$lang['xemchitiet'] = "Xem thêm";
$lang['tinlienquan'] = "Tin liên quan";
$lang['ungtuyen'] = "Ứng tuyển";

$lang['dangcapnhat'] = "Đang cập nhật ...";




