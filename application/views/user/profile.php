<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="dangnhap-in clearfix">
            <form class="form-dangky" action="" method="POST">
                <h2>Tạo mới tài khoản</h2>
                <div class="msg_box_error">
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                </div>
                <div class="clearfix">
                    <label>
                        <span class="req">*</span>Họ tên:</label>
                    <input name="name" maxlength="150" type="text" value="<?php echo set_value('name'); ?>">
                </div>
                <div class="clearfix">
                    <label>
                        <span class="req">*</span>Điện thoại:</label>
                    <input name="phone" maxlength="25" type="text" value="<?php echo set_value('phone'); ?>">
                </div>
                <div class="clearfix">
                    <label>
                        <span class="req">*</span>Email của bạn:
                    </label>
                    <input name="email" maxlength="100" type="text" value="<?php echo set_value('email'); ?>">
                </div>
                <div class="clearfix">
                    <label>
                        Địa chỉ:</label>
                    <input name="address" maxlength="250" type="text" value="<?php echo set_value('address'); ?>">
                </div>
                <div class="clearfix">
                    <label>
                        <span class="req">*</span>Mật khẩu:
                    </label>
                    <input name="pas" maxlength="20" type="password">
                </div>
                <div class="clearfix" style="text-align: center">
                    <input value="Tạo tài khoản" type="submit" class="button-dk">
                </div>
            </form>
            <form class="form-dangnhap" method="POST" action="login">
                <h2>Đăng nhập</h2>
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>
                <div class="clearfix">
                    <label>
                        <span class="req">*</span>Email của bạn:
                    </label>
                    <input name="email" maxlength="100" type="text">
                </div>
                <div class="clearfix">
                    <label>
                        <span class="req">*</span>Mật khẩu:
                    </label>
                    <input name="pas" maxlength="20" type="password">
                </div>
                <div class="clearfix" style="text-align: center">
                    <input value="Đăng nhập" type="submit" class="button-dn">
                </div>
            </form>
        </div>
    </div>
</section>