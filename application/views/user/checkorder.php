<?php
if (count($list) > 0) {
    ?>
    <table width="100%" class="table table-bordered">
        <tr>
            <td>STT</td>
            <td>Mã đơn hàng</td>
            <td>Thời gian đặt</td>
            <td style="width: 100px;">Người nhận</td>
            <td>Địa chỉ người nhận</td>
        </tr>
        <?php
        $i = 1;
        foreach ($list as $row) {
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['code']; ?></td>
                <td><?php echo date("d/m/Y H:i:s", strtotime($row['time_order'])); ?></td>
                <td><?php echo $row['name_nguoinhan']; ?></td>
                <td><?php echo $row['diachi_nguoinhan']; ?></td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </table>
    <?php
} else {
    echo "<center>Không tìm thấy dữ liệu</center>";
}
?>
