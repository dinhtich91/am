<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>Nhaam.com</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">

        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="intro-background clearfix">
        <div class="container clearfix">
            <div class="row clearfix disable-margin">
                <div class="col-md-12 disable-padding">
                    <div class="intro-box">
                        <div class="row disable-margin clearfix intro-row-block">
                            <div class="col-md-9 disable-padding clearfix intro-left-block">
                                <div class="logo-intro-box">
                                    <img src="img/logo-nhaam.png" alt="logo-nhaam" title="logo-nhaam" />
                                </div>
                                <div class="intro-text-box">
                                    <h3 class="tahoma-font-bold disable-margin-top intro-headding">NƠI KỂ VỀ CUỘC ĐỜI <br> & NỖI ĐAM MÊ CỦA NGƯỜI THỢ XÂY</h3>
                                    <div class="text-intro-detail">
                                        <p class="text-slogan-intro">Nhà Ấm đặt mình cạnh ước mơ của bạn, cùng bạn xây dựng<br> giấc mơ chung về một không gian “sớm tối đi về”. <br> Bao khó nhọc của việc mưu sinh, càng làm chúng ta <br> nhận ra rõ hơn giá trị cuộc sống.</p>
                                    </div>
                                </div>
                                <div class="multilanguage-box-intro clearfix">
                                    <a href="#"><img src="img/vn-flag.png" alt="vn-flag" title="vn-flag" /></a>
                                    <a href="#"><img src="img/usa-flag.png" alt="usa-flag" title="usa-flag" /></a>
                                    <a href="#"><img src="img/jp-flag.png" alt="jp-flag" title="jp-flag" /></a>
                                </div>
                            </div>
                            <div class="col-md-3 disable-padding clearfix intro-right-block clearfix">
                                <div class="menu-intro-popup clearfix">
                                    <div class="intro-menu-left">
                                        <div class="intro-home-menu-background">
                                            <a href="<?php echo base_url(); ?>" class="intro-home-link">
                                                <span class="glyphicon glyphicon-home home-icon"></span>
                                                <span class="text-uppercase tahoma-font home-text-icon">Trang chủ</span>
                                            </a>
                                        </div>
                                        <div class="big-block">
                                            <div class="block-content cleafix">
                                                <div class="row disable-margin flexi-row">
                                                    <div class="col-md-6 disable-padding clearfix">
                                                        <a class="intro-link-height"><img class="img-max-width" src="img/intro-aboutus.png" alt="about-us" title="about-us"/></a>
                                                    </div>
                                                    <div class="col-md-6 clearfix disable-padding intro-padding">
                                                        <a class="intro-link-height" href="<?php echo base_url('gioi-thieu'); ?>"><p class="menu-text-intro disable-margin-bottom">Về chúng tôi</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block-content clearfix">
                                                <div class="row disable-margin flexi-row">
                                                    <div class="col-md-6 clearfix disable-padding">
                                                        <a class="intro-link-height"><img class="img-max-width" src="img/intro-nhaamdandung.png" alt="dan-dung" title="dan-dung"/></a>
                                                    </div>
                                                    <div class="col-md-6 clearfix disable-padding intro-padding">
                                                        <a class="intro-link-height" href="<?php echo base_url('nha-am-dan-dung'); ?>"><p class="menu-text-intro disable-margin-bottom">Nhà ấm dân dụng</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block-content clearfix">
                                                <div class="row disable-margin flexi-row">
                                                    <div class="col-md-6 clearfix disable-padding">
                                                        <a class="intro-link-height"><img class="img-max-width" src="img/intro-nhaamcn.png" alt="cong-nghiep" title="cong-nghiep"/></a>
                                                    </div>
                                                    <div class="col-md-6 clearfix disable-padding intro-padding">
                                                        <a class="intro-link-height" href="<?php echo base_url('nha-am-cong-nghiep'); ?>"><p class="menu-text-intro disable-margin-bottom">Nhà ấm công nghiệp</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block-content cleafix">
                                                <div class="row disable-margin flexi-row">
                                                    <div class="col-md-6 disable-padding clearfix">
                                                        <a class="intro-link-height"><img class="img-max-width" src="img/intro-maunha.png" alt="mau-nha" title="mau-nha"/></a>
                                                    </div>
                                                    <div class="col-md-6 clearfix disable-padding intro-padding">
                                                        <a class="intro-link-height"><p class="menu-text-intro disable-margin-bottom">Mẫu nhà</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block-content clearfix">
                                                <div class="row disable-margin flexi-row">
                                                    <div class="col-md-6 clearfix disable-padding">
                                                        <a class="intro-link-height"><img class="img-max-width" src="img/intro-tintuc.png" alt="tin-tuc" title="tin-tuc"/></a>
                                                    </div>
                                                    <div class="col-md-6 clearfix disable-padding intro-padding">
                                                        <a class="intro-link-height" href="<?php echo base_url('tin-tuc'); ?>"><p class="menu-text-intro disable-margin-bottom">Tin tức</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block-content clearfix">
                                                <div class="row disable-margin flexi-row">
                                                    <div class="col-md-6 clearfix disable-padding">
                                                        <a class="intro-link-height"><img class="img-max-width" src="img/intro-lienhe.png" alt="lien-he" title="lien-he"/></a>
                                                    </div>
                                                    <div class="col-md-6 clearfix disable-padding intro-padding">
                                                        <a class="intro-link-height" href="<?php echo base_url('lien-he'); ?>"><p class="menu-text-intro disable-margin-bottom">Liên hệ</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /container -->
    </body>
</html>
