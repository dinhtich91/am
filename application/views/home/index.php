<?php 
    $slider_home = $this->fromm->slider_home();
    $slider_nhatieubieu = $this->fromm->slider_nhatieubieu();
?>
<div class="container-fluid disable-padding background-home">
            <div class="container nhaam-margin-top">
                <div class="row flexi-row">
                    <div class="col-md-1-5 disable-padding">
                        <div class="side-bar">
                            <div class="home-block">
                                <a href="http://www.nhaam.com.vn/"><img src="img/mau-nha-side.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                            <div class="home-block">
                                <a href="javascript:void(0);" class="clip1-nhaam"><img src="img/video-clip-nhaam.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                            <div class="home-block">
                                <a href="#"><img src="img/green-house.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                            <div class="home-block">
                                <a href="http://www.nhaam.com/"><img src="img/dia-oc.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                            <div class="home-block">
                                <a href="#"><img src="img/ca-phe.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                            <div class="home-block">
                                <a href="http://www.enoithat.vn/"><img src="img/sieu-thi.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                            <div class="home-block">
                                <a href="javascript:void(0);" class="clip2-nhaam"><img src="img/robot.png" alt="mau-nha" title="mau-nha" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9-6">
                        <div class="content-block-box clearfix">
                            <div class="row disable-margin">
                                <div class="col-md-12 disable-padding">
                                    <div class="about-us-home-box">
                                        <a class="about-link-home">
                                            <div class="clearfix">
                                                <h3 class="disable-margin text-uppercase heading-slogan open-regular">chữ tín quý hơn vàng</h3>
                                                <p class="about-slogan-txt open-regular">Là bất cứ ai trong chúng ta đều nuôi dưỡng ước mơ có một không gian sống và <br> muốn nó luôn đẹp hơn, hoàn hảo hơn.</p>
                                                <button class="read-more-btn open-regular">Xem thêm</button>
                                            </div> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row disable-margin">
                                <div class="home-slider-2">
                                    <ul class="slides">
                                        <?php
                                            if(isset($slider_home) && !empty($slider_home)){
                                                foreach ($slider_home as $key => $value) {
                                                    ?>
                                                        <li>
                                                            <a href="<?php echo $value['link'] ?>">
                                                                <img class="slider-2" src="<?php echo $value['image'] ?>" alt="<?php echo $value['title_'.$lang]; ?>" title="<?php echo $value['title_'.$lang]; ?>" />
                                                            </a>
                                                        </li>
                                                    <?php
                                                }
                                            }

                                        ?>
                                    </ul>
                                    <div class="custom-navigation">
                                        <div class="custom-controls-container"></div>
                                    </div>
                                </div>                               
                            </div>
                            <div class="row disable-margin category-margin-top">
                                <div class="col-md-6">
                                    <div class="category-box am-dan-dung">
                                        <img src="img/category-1.png" alt=""  title=""/>
                                        <div class="category-box-sup">
                                            <p class="text-uppercase open-bold category-text-sup">nhà ấm dân dụng</p>
                                        </div>
                                        <div class="category-box-img">
                                            <img class="logo-mini-nhaam-box" src="img/nhaam-mini.png" alt="nhaam" title="nhaam" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 disable-padding-left">
                                    <div class="category-box am-cong-nghiep">
                                        <img src="img/category-2.png" alt=""  title=""/>
                                        <div class="category-box-sup">
                                            <p class="text-uppercase open-bold category-text-sup">nhà ấm công nghiệp</p>
                                        </div>
                                        <div class="category-box-img">
                                            <img class="logo-mini-nhaam-box" src="img/am-con-mini.png" alt="amcon" title="amcon" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container margin-for-slider">
                <div class="row">
                    <div class="home-slider-3">
                        <ul class="slides">

                            <?php
                                if(isset($slider_nhatieubieu) && !empty($slider_nhatieubieu)){
                                    foreach ($slider_nhatieubieu as $key => $value) {
                                        ?>
                                            <li class="slide-father-caption">
                                                <a href="<?php echo $value['link'] ?>">
                                                    <img class="slider-2" src="<?php echo $value['image'] ?>" alt="<?php echo $value['title_'.$lang]; ?>" title="<?php echo $value['title_'.$lang]; ?>" />
                                                </a>
                                                <div class="category-box-sup re-width">
                                                    <p class="open-regular category-text-sup font-descrease"><?php echo $value['title_'.$lang]; ?>"</p>
                                                </div>
                                            </li>
                                        <?php
                                    }
                                }

                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

<script>
 jQuery(document).ready(function ($) {
    $('.clip1-nhaam').on('click', function () {
        $.showYtVideo({
            videoId: 'nBo0IgKrPig'
        });
    });
    $('.clip2-nhaam').on('click', function () {
        $.showYtVideo({
            videoId: 'N2LymZJG31w'
        });
    });
    $('.about-us-home-box').click(function(){
        window.location.href="<?php echo base_url('gioi-thieu'); ?>";
    });
    $('.am-dan-dung').click(function(){
        window.location.href="<?php echo base_url('nha-am-dan-dung'); ?>";
    });
    $('.am-cong-nghiep').click(function(){
        window.location.href="<?php echo base_url('nha-am-cong-nghiep'); ?>";
    });

    
});
</script>
