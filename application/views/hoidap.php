<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <h3 class="contact-heading-background open-san-semibold">
                                <?php
                                    echo $this->lang->line('hoidap');  
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        
                        <div class="col-sm-12 main disable-padding get-height" style="margin-left:0px; ">
                            <div class="row disable-margin product-details-box" style="padding-bottom: 30px;">
                                <div class="col-sm-12">

                                    <div class="panel-group" id="accordion" style="border:none;">
                                      <?php
                                        if(isset($list) && !empty($list)){
                                          foreach ($list as $key => $value) {
                                          ?>


                                          <div class="panel panel-default hoidap-1" >
                                          <div class="panel-heading hoidap-2" >
                                            <span class="hoidap-3"><?php echo $key+1 ?></span>
                                            <h4 class="panel-title hoidap-4">
                                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>"  class="hoidap-5"><?php echo strip_tags($value['cauhoi_'.$lang]) ?></a>
                                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>" class="hoidap-6"><img src="img/mui-ten.png" /></a>
                                            </h4>
                                          </div>
                                          <div id="collapse<?php echo $key ?>" class="panel-collapse collapse">
                                            <div class="panel-body" style="padding-left:0px; padding-right:0px;">
                                            <span class="hoidap-7" >Đáp</span>
                                            <span class="hoidap-8"><?php echo strip_tags($value['traloi_'.$lang]) ?></span>
                                            </div>
                                          </div>
                                        </div>

                                        <!-- <div class="panel panel-default hoidap-1 clearfix">
                                          <div class="panel-heading hoidap-2">
                                            <h4 class="panel-title hoidap-4">
                                            <span class="hoidap-3"><?php echo $key+1 ?></span>
                                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>" class="hoidap-5">
                                                <?php echo strip_tags($value['cauhoi_'.$lang]) ?>
                                              </a>

                                            </h4>
                                          </div>
                                          <div id="collapse<?php echo $key ?>" class="panel-collapse collapse">
                                            <div class="panel-body" style="padding-left:0px; padding-right:0px;">
                                              <span class="hoidap-7" >Đáp</span>
                                              <span class="hoidap-8"><?php echo strip_tags($value['traloi_'.$lang]) ?></span>
                                            </div>
                                          </div>
                                        </div> -->
                                          <?php
                                          }
                                        }
                                      ?>
                                        
                                        
                                        

                                      </div> 
                                  
                                </div>                                     
                            </div>
                    </div>
                </div>
            </div>            
        </div> <!-- /container -->

<style type="text/css">
    .product-details-box{
        padding: 30px 40px;
    }
    
    .img-doitac{
        text-align: center;
    }
    .title-doitac{
        text-align: center;
        text-transform: uppercase;
    }

    .hoidap-1{
      border:none; box-shadow: none; border-radius: 0px; margin-top:0px;
    }
    .hoidap-2{
      padding-top:0px; padding-bottom:0px;
    }
    .hoidap-3{
      display:inline-block; width:8.8%; height:90px; background-color:#f1b22a; color:#fff; text-align: center; font-size:33px; padding-top:20px; font-weight:bold; float:left;
    }
    .hoidap-4{
      width:91.2%; height:90px; display:inline-block; float:left; background-color:#fdf4df; margin-bottom: 5px;
    }

    .hoidap-5{
      width:80%; display:inline-block; height:90px; padding-left:20px; padding-top:40px; text-transform: uppercase; color:#f1b22a !important; font-weight:bold; font-size:19px; float:left;
    }
    .hoidap-6{
      width:20%; display:inline-block; height:90px; padding-top:40px; float:left; text-align: right; padding-right:30px;
    }
    .hoidap-7{
      display:inline-block; width:8.9%; height:90px; background-color:#f5f5f5; color:#000; text-align: center; font-size:40px; padding-top:15px; font-weight:bold; float:left;
      padding-left: 15px;
    }
    .hoidap-8{
      width:91%; display:inline-block; min-height:90px; padding-left:20px; padding-top:20px; color:#000; background-color:#f5f5f5; font-size:14px; float:left;
    }



</style>
