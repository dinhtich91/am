<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="quatang-in clearfix">
            <div class="step clearfix">
                <ul class="clearfix">
                    <li class="active">
                        <a href="javascript:void(0);">1</a>
                        <span>Quà tặng kèm</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">2</a>
                        <span>Thông tin liên lạc </span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">3</a>
                        <span>Thông tin người nhận</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">4</a>
                        <span>Lời nhắn </span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">5</a>
                        <span>Thanh toán</span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">6</a>
                        <span>Hoàn tất</span>
                    </li>
                </ul>
            </div>
            <div class="quatang-w clearfix">
                <div class="quatang-l clearfix">
                    <form action="cart/update" method="post" accept-charset="utf-8">
                        <div class="block-giohang">
                            <h2>GIỎ HÀNG</h2>
                            <?php
                            $j = 0;
                            foreach ($this->cart->contents() as $items):
                                echo form_hidden($j . '[rowid]', $items['rowid']);
                                $detailProduct = $this->gaucon->detailProductByID($items['id']);
                                $title = $detailProduct['title'];
                                $link = $this->gaucon->permartlinkProduct($detailProduct['id']);
                                ?>
                                <div class="item-giohang clearfix">
                                    <div class="item-giohang-i">
                                        <a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $detailProduct['avatar']; ?>" alt=""></a>
                                    </div>
                                    <div class="item-giohang-t">
                                        <h4><a href="<?php echo $link; ?>" target="_blank"><?php echo $title; ?></a></h4>
                                        <p><span>Số lượng:</span>
                                            <select name="<?= $j . '[qty]'; ?>" onchange="$('form').submit();">
                                                <?php
                                                for ($i = 1; $i <= 20; $i++) {
                                                    ?>
                                                    <option <?= $items['qty'] == $i ? "selected=selected" : ""; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </p>
                                        <p><span>Đơn giá:</span> <?php echo number_format($items['subtotal']) ?> vnđ</p>
                                    </div>
                                    <a href="<?php echo site_url("xoa-san-pham/" . $items['rowid']); ?>" class="item-giohang-c"><img src="img/close.png" height="12" width="9" alt=""></a>
                                </div>
                                <?php
                                $j++;
                            endforeach;
                            ?>
                            <?php
                            if (count($this->cart->contents()) > 0) {
                                ?>
                                <div class="item-giohang-total clearfix">
                                    <div class="item-giohang-t">
                                        <p>Gấu Con sẽ Free Ship ở các khu vực : QUẬN 1, QUẬN 3, QUẬN 10 và với giá trị đơn hàng trên 500.000 vnđ. Và đối với những khu vực lân cận sẽ tùy theo đơn hàng và tùy theo khu vực Gấu Con sẽ đưa ra mức phí phù hợp nhất, để đảm bảo quyền lợi của khách hàng.</p>
                                        <p><span>Tổng :</span> <?php echo number_format($this->cart->total()); ?> vnđ</p>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <em>Giỏ hàng trồng, vui lòng chọn sản phẩm!</em>
                            <?php } ?>
                        </div>
                    </form>
                </div>
                <div class="quatang-r clearfix">
                    <div class="block-thongtin">
                        <div class="ln">
                            <h2>LỜI NHẮN</h2>
                            <form action="" method="post">
                                <p class="clearfix">
                                    <span>Thiệp gửi tặng: </span>
                                    <?php
                                    $tangcho = $this->session->userdata("tangcho") ? $this->session->userdata("tangcho") : 0;
                                    ?>
                                    <select name="tangcho">
                                        <option <?= $tangcho == 1 ? "selected" : ""; ?> value="1">Anh, em - Brother</option>
                                        <option <?= $tangcho == 2 ? "selected" : ""; ?> value="2">Bạn bè - Friends</option>
                                        <option <?= $tangcho == 3 ? "selected" : ""; ?> value="3">Bố Mẹ - Parent</option>
                                        <option <?= $tangcho == 4 ? "selected" : ""; ?> value="4">Chồng - Husband</option>
                                        <option <?= $tangcho == 5 ? "selected" : ""; ?> value="5">Đố tác, khách hàng - Partners</option>
                                        <option <?= $tangcho == 6 ? "selected" : ""; ?> value="6">Đồng nghiệp - Co-worker</option>
                                        <option <?= $tangcho == 7 ? "selected" : ""; ?> value="7">Người yêu - Lover</option>
                                        <option <?= $tangcho == 7 ? "selected" : ""; ?> value="8">Vợ -Wife</option>
                                        <option <?= $tangcho == 0 ? "selected" : ""; ?> value="0">Khác - Other</option>
                                    </select>
                                </p>
                                <p class="clearfix">
                                    <span>Nhân dịp: </span>
                                    <?php
                                    $nhandip = $this->session->userdata("nhandip") ? $this->session->userdata("nhandip") : 0;
                                    ?>
                                    <select name="nhandip">
                                        <option <?= $nhandip == 1 ? "selected" : ""; ?> value="1">Cảm ơn</option>
                                        <option <?= $nhandip == 2 ? "selected" : ""; ?> value="2">Chia buồn</option>
                                        <option <?= $nhandip == 3 ? "selected" : ""; ?> value="3">Chúc mừng</option>
                                        <option <?= $nhandip == 4 ? "selected" : ""; ?> value="4">Khai trương</option>
                                        <option <?= $nhandip == 5 ? "selected" : ""; ?> value="5">Làm quen - Dating</option>
                                        <option <?= $nhandip == 6 ? "selected" : ""; ?> value="6">Ngày kỉ niệm - Anniversary</option>
                                        <option <?= $nhandip == 7 ? "selected" : ""; ?> value="7">Ngày lễ (20/10 - 08/03)</option>
                                        <option <?= $nhandip == 8 ? "selected" : ""; ?> value="8">Sinh nhật - Birthday</option>
                                        <option <?= $nhandip == 0 ? "selected" : ""; ?> value="0">Khác</option>
                                    </select>
                                </p>
                                <p class="pline"></p>
                                <p class="clearfix"><span>Thông điệp: </span><textarea name="thongdiep" placeholder="Nhập thông điệp của bạn ...."><?php echo $this->session->userdata("thongdiep"); ?></textarea></p>
                                <p class="clearfix"><span>Lời nhắn cho<br/>Gấu Con shop: </span><textarea name="msg_gaucon" placeholder="Nhập lời nhắn của bạn ...."><?php echo $this->session->userdata("msg_gaucon"); ?></textarea></p>
                                <p class="clearfix temp"><input type="submit" value="Tiếp tục" class="submit-form"><i class="fa fa-angle-double-right"></i></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>