<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="quatang-in clearfix">
            <div class="step clearfix">
                <ul class="clearfix">
                    <li class="active">
                        <a href="javascript:void(0);">1</a>
                        <span>Quà tặng kèm</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">2</a>
                        <span>Thông tin liên lạc </span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">3</a>
                        <span>Thông tin người nhận</span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">4</a>
                        <span>Lời nhắn </span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">5</a>
                        <span>Thanh toán</span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">6</a>
                        <span>Hoàn tất</span>
                    </li>
                </ul>
            </div>
            <div class="quatang-w clearfix">
                <div class="quatang-l clearfix">
                    <form action="cart/update" method="post" accept-charset="utf-8">
                        <div class="block-giohang">
                            <h2>GIỎ HÀNG</h2>
                            <?php
                            $j = 0;
                            foreach ($this->cart->contents() as $items):
                                echo form_hidden($j . '[rowid]', $items['rowid']);
                                $detailProduct = $this->gaucon->detailProductByID($items['id']);
                                $title = $detailProduct['title'];
                                $link = $this->gaucon->permartlinkProduct($detailProduct['id']);
                                ?>
                                <div class="item-giohang clearfix">
                                    <div class="item-giohang-i">
                                        <a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $detailProduct['avatar']; ?>" alt=""></a>
                                    </div>
                                    <div class="item-giohang-t">
                                        <h4><a href="<?php echo $link; ?>" target="_blank"><?php echo $title; ?></a></h4>
                                        <p><span>Số lượng:</span>
                                            <select name="<?= $j . '[qty]'; ?>" onchange="$('form').submit();">
                                                <?php
                                                for ($i = 1; $i <= 20; $i++) {
                                                    ?>
                                                    <option <?= $items['qty'] == $i ? "selected=selected" : ""; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </p>
                                        <p><span>Đơn giá:</span> <?php echo number_format($items['subtotal']) ?> vnđ</p>
                                    </div>
                                    <a href="<?php echo site_url("xoa-san-pham/" . $items['rowid']); ?>" class="item-giohang-c"><img src="img/close.png" height="12" width="9" alt=""></a>
                                </div>
                                <?php
                                $j++;
                            endforeach;
                            ?>
                            <?php
                            if (count($this->cart->contents()) > 0) {
                                ?>
                                <div class="item-giohang-total clearfix">
                                    <div class="item-giohang-t">
                                        <p>Gấu Con sẽ Free Ship ở các khu vực : QUẬN 1, QUẬN 3, QUẬN 10 và với giá trị đơn hàng trên 500.000 vnđ. Và đối với những khu vực lân cận sẽ tùy theo đơn hàng và tùy theo khu vực Gấu Con sẽ đưa ra mức phí phù hợp nhất, để đảm bảo quyền lợi của khách hàng.</p>
                                        <p><span>Tổng :</span> <?php echo number_format($this->cart->total()); ?> vnđ</p>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <em>Giỏ hàng trồng, vui lòng chọn sản phẩm!</em>
                            <?php } ?>
                        </div>
                    </form>
                </div>
                <div class="quatang-r clearfix">
                    <div class="block-thongtin">
                        <div class="ll">
                            <h2>THÔNG TIN LIÊN LẠC</h2>
                            <div class="msg_box_error">
                                <?php echo validation_errors('<div class="error">', '</div>'); ?>
                            </div>
                            <form action="" method="post">
                                <p class="clearfix"><span>Họ và tên <span>*</span></span><input type="text" name="name" value="<?= $name != "" ? $name : set_value('name'); ?>"></p>
                                <p class="clearfix"><span>Điện thoại <span>*</span></span><input type="text" name="phone" value="<?= $phone != "" ? $phone : set_value('phone'); ?>"></p>
                                <p class="clearfix"><span>Email</span><input type="text" name="email" value="<?= $email != "" ? $email : set_value('email'); ?>"></p>
                                <p class="clearfix"><span>Địa chỉ <span>*</span></span><input type="text" name="address" value="<?= $address != "" ? $address : set_value('address'); ?>"></p>
                                <p class="clearfix temp"><input type="submit" name="" value="Tiếp tục" class="submit-form"> <i class="fa fa-angle-double-right"></i></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>