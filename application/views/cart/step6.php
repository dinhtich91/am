<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="quatang-in clearfix">
            <div class="step clearfix">
                <ul class="clearfix">
                    <li class="active">
                        <a href="javascript:void(0);">1</a>
                        <span>Quà tặng kèm</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">2</a>
                        <span>Thông tin liên lạc </span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">3</a>
                        <span>Thông tin người nhận</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">4</a>
                        <span>Lời nhắn </span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">5</a>
                        <span>Thanh toán</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">6</a>
                        <span>Hoàn tất</span>
                    </li>
                </ul>
            </div>
            <div class="quatang-w clearfix">
                <h1>Xác nhận đơn hàng thành công</h1>
                <p>Cảm ơn Quý khách đã đặt hàng tại Shop Hoa Tươi <a href="http://gauconflower.com/">gauconflower.com</a> xác nhận đã nhận được đơn hàng của của quý khách theo thông tin bên dưới. Nếu Quý khách thắc mắc trong việc mua hàng, Quý khách đừng ngần ngại liên lạc với chúng tôi theo các phương thức sau:
                <p>1. Chat với Hoa Tươi Gấu Con bằng khung chat ở góc phải dưới màn hình của Quý khách</p>
                <p>2. Liên hệ với Hoa Tươi Gấu Con qua email: <a href="mailto:gauconflower@gmail.com">gauconflower@gmail.com</a></p>
                <p>3. Gọi điện thoại trực tiếp cho Hoa Tươi Gấu Con theo số điện thoại: 08.62908567 - 0909.306.952 - 0909.300.952</p>
                <p><br/>Hoa tươi Gấu Con xin một lần nữa cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ hoa tươi của Chúng tôi. Nếu có sự bất tiện hoặc lỗi hệ thống xin Quý khách thông cảm cho Chúng tôi.</p>
                </p>
                <table class="tb_order_info table-bordered" border="0" cellpadding="0" cellspacing="1">
                    <tbody>
                        <tr>
                            <th style="width: 50%" align="left" valign="middle">
                                <strong>Mã đơn hàng: <?php echo $this->session->userdata("CODE_CART"); ?></strong>
                            </th>
                            <th style="width: 50%;" align="right" valign="top">
                                <strong>Thời gian mua hàng: </strong> <?php echo date("d/m/Y"); ?>
                                <?php
                                $day = $this->session->userdata("day");
                                $month = $this->session->userdata("month");
                                $year = $this->session->userdata("year");
                                $time = $this->session->userdata("time");
                                ?>
                                <br><strong>Ngày giao hàng: </strong> <?php echo $day; ?>/<?php echo $month; ?>/<?php echo $year; ?> vào lúc <?php echo $time; ?>h
                            </th>
                        </tr>
                        <tr>
                            <th align="center" valign="middle">
                                Thông tin liên lạc
                            </th>
                            <th align="center" valign="middle">
                                Thông tin người nhận
                            </th>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">
                                <strong class="title">Họ tên: </strong> <?php echo $this->session->userdata("name"); ?>
                            </td>
                            <td align="left" valign="middle">
                                <strong class="title">Họ tên: </strong> <?php echo $this->session->userdata("name_nguoinhan"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">
                                <strong class="title">Địa chỉ: </strong> <?php echo $this->session->userdata("address"); ?>
                            </td>
                            <td align="left" valign="middle">
                                <strong class="title">Địa chỉ: </strong> <?php echo $this->session->userdata("address_nguoinhan"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">
                                <strong class="title">Điện thoại: </strong> <?php echo $this->session->userdata("phone"); ?>
                            </td>
                            <td align="left" valign="middle">
                                <strong class="title">Điện thoại: </strong> <?php echo $this->session->userdata("phone_nguoinhan"); ?>
                            </td>
                        </tr>
                        <?php
                        $district = $this->gaucon->getName("name", "district", "id", $this->session->userdata("district"));
                        $city = $this->gaucon->getName("name", "city", "id", $this->session->userdata("city"));
                        ?>
                        <tr>
                            <td align="left" valign="middle">&nbsp;</td>
                            <td align="left" valign="middle">
                                <strong class="title">
                                    Quận/Huyện: </strong> <?php echo $district; ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">&nbsp;</td>
                            <td align="left" valign="middle">
                                <strong class="title">Tỉnh/Thành phố: </strong> <?php echo $city; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="tb_order_info table-bordered margin_top_15" border="0" cellpadding="0" cellspacing="1">
                    <tbody>
                        <tr>
                            <th style="width: 20px;" align="center">
                                STT
                            </th>
                            <th align="center">
                                Tên sản phẩm
                            </th>
                            <th style="width: 120px;" align="center">
                                Image
                            </th>
                            <th style="width: 15%;" align="center">
                                Đơn giá
                            </th>
                            <th style="width: 15%;" align="center">
                                Số lượng
                            </th>
                            <th style="width: 15%;" align="center">
                                Tạm tính
                            </th>
                        </tr>
                        <?php
                        $j = 0;
                        foreach ($this->cart->contents() as $items):
                            echo form_hidden($j . '[rowid]', $items['rowid']);
                            $detailProduct = $this->gaucon->detailProductByID($items['id']);
                            $title = $detailProduct['title'];
                            $link = $this->gaucon->permartlinkProduct($detailProduct['id']);
                            ?>
                            <tr>
                                <td align="center" valign="middle">
                                    <?php echo $j + 1; ?>
                                </td>
                                <td align="left"><?php echo $title; ?></td>
                                <td align="center">
                                    <a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $detailProduct['avatar']; ?>" alt=""></a>
                                </td>
                                <td align="center">
                                    <?php echo number_format($items['price']) ?> vnđ
                                </td>
                                <td align="center">
                                    <?php echo $items['qty']; ?>
                                </td>
                                <td align="right">
                                    <?php echo number_format($items['subtotal']) ?> vnđ
                                </td>
                            </tr>
                            <?php
                            $j++;
                        endforeach;
                        ?>
                        <tr>
                            <td align="right" valign="middle">
                                <strong>Tổng:</strong>
                            </td>
                            <td colspan="5" align="right" valign="middle">
                                <strong><?php echo number_format($this->cart->total()); ?> vnđ</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p style="margin-bottom: 10px;font-style: italic;color: #03f;">Gấu Con sẽ Free Ship ở các khu vực : QUẬN 1, QUẬN 3, QUẬN 10 và với giá trị đơn hàng trên 500.000 vnđ. Và đối với những khu vực lân cận sẽ tùy theo đơn hàng và tùy theo khu vực Gấu Con sẽ đưa ra mức phí phù hợp nhất, để đảm bảo quyền lợi của khách hàng.</p>
                <h2>THÔNG TIN CHUYỂN KHOẢN</h2>
                <p>Chủ tài khoản : Lê Thị Thu Trang</p>
                <p>1. Ngân Hàng Thương Mại Cổ Phần Ngoại Thương VN - Chi Nhánh TP.HCM (VCB) : 007.1003.711.760</p>
                <p>2. Ngân Hàng Thương Mại Cổ Phần Đông Á : 0103.253.151</p>
                <p>3. Ngân Hàng Thương Mại Cổ Phần Á Châu (ACB) : 2053.27129</p>
            </div>
        </div>
    </div>
</section>