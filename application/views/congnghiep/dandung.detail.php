<div class="container-fluid disable-padding background-home">            
            <div class="container disable-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-content">
                            <ul class="nav nav-tabs navtab-nhaam">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li><a class="text-uppercase open-regular <?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>" href="<?php echo site_url('nha-am-dan-dung/'.$value['tag']); ?>">
                                                 <?php echo $value['title_'.$lang]; ?>
                                                </a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>

                            <div class="">
                                <div id="home" class="">
                                    <div class="row disable-margin">
                                        <div id='gallery_temp'>
                                        <?php
                                            if(isset($dandung_img) && !empty($dandung_img)){
                                                foreach ($dandung_img as $key => $value) {
                                                    ?>
                                                        <div class="col-md-3 gian-cach">
                                                            <a href="upload/album/lagre/<?php echo $value['image']; ?>">
                                                                <img class="thumb-nail-img" src="upload/album/thumb/<?php echo $value['image'] ?>" />
                                                            </a>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>

<script>

    // applying photobox on a `gallery` element which has lots of thumbnails links.
      // Passing options object as well:
      //-----------------------------------------------
      $('#gallery_temp').photobox('a',{ time:0 });

      // using a callback and a fancier selector
      //----------------------------------------------
      $('#gallery_temp').photobox('li > a.family',{ time:0 }, callback);
      function callback(){
         console.log('image has been loaded');
      }

      // // destroy the plugin on a certain gallery:
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('destroy');

      // // re-initialize the photbox DOM (does what Document ready does)
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('prepareDOM');
  </script>