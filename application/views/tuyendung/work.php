<div class="bao-work">
	<div class="title-work">
		<?php
			echo $work['title_'.$lang];
		?>
	</div>
	<?php
		if(isset($work) && !empty($work)){
			echo $work['detail_'.$lang];
		}
		else{
			echo 'No data';
		}
	?>
</div>
<style type="text/css">
	/*#facebox .content{
		width: 600px;		
	}*/
	#facebox {
		height: 600px;
		overflow-y: scroll;
	}

	#facebox .popup{
		border-right: 0px;
		border-top: 0px;
	}
	#facebox .close img{
		opacity: 1;
	}
	.title-work{
		text-transform: uppercase;
	    font-size: 20px;
	    font-weight: bold;
	    color: #fdb71e;
	    text-align: center;
	    padding-bottom: 30px;
	}
</style>
