<?php
    $allFooter = $this->fromm->getFooter();
?>
<div class="container-fluid disable-padding">
            <footer class="footer-background-color padding-top-footer">
                <div class="container disable-padding address-box">
                    <div class="row disable-margin">
                        <div class="col-md-2 disable-padding">
                            <?php
                                if(isset($allFooter['title_'.$lang])){
                                    echo $allFooter['title_'.$lang];
                                }
                            ?>
                        </div>
                        <div class="col-md-3 disable-padding-left">
                            <?php
                                if(isset($allFooter['footer1_'.$lang])){
                                    echo $allFooter['footer1_'.$lang];
                                }
                            ?>
                        </div>
                        <div class="col-md-4 disable-padding-left">
                            <?php
                                if(isset($allFooter['footer2_'.$lang])){
                                    echo $allFooter['footer2_'.$lang];
                                }
                            ?>
                        </div>
                        <div class="col-md-3 disable-padding-right">
                            <?php
                                if(isset($allFooter['footer3_'.$lang])){
                                    echo $allFooter['footer3_'.$lang];
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="container disable-padding">
                    <div class="row disable-margin row-desktop">
                        <div class="col-md-12 disable-padding">
                            <p class="roboto-light white-text">Copyright © 2016 nhaam.com. All rights reserved | Designed By SGD </p>
                        </div>
                    </div>
                    <div class="row disable-margin row-mobile">
                        <div class="col-md-6 disable-padding">
                            <p class="roboto-light">Copyright © 2016 nhaam.com. All rights reserved </p>
                        </div>
                        <div class="col-md-6 disable-padding">
                            <p class="roboto-light">&nbsp;| Designed By SGD</p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>