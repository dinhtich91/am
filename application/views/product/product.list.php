<link rel="stylesheet" href="css/animate.min.css" />
<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1><?php echo $title_header; ?></h1>
        <div class="list-products-in clearfix">
            <?php
            $i = 1;
            if (count($list) > 0) {
                foreach ($list as $row) {
                    $percent = 0;
                    $link = site_url($tag_parent . "/" . $tag_child . "/" . $row['tag']);
                    $sale = $row['sale'] > 0 ? number_format($row['sale']) . " đ" : 0;
                    $price = $row['price'] > 0 ? number_format($row['price']) . " đ" : "Liên hệ";
                    if ($sale > 0) {
                        $percent = round(($sale / $price) * 100, 0);
                    }
                    ?>
                    <div class="list-products-item wow fadeInUp" data-wow-delay="<?= 0.001 * (5 * $i); ?>s">
                        <div class="item-in" href="<?php echo $link; ?>" title="<?php echo $row['title']; ?>">
                            <h2><a href="<?php echo $link; ?>" title="<?php echo $row['title']; ?>"><?php echo $row['title']; ?></a></h2>
                            <a href="<?php echo $link; ?>" title="<?php echo $row['title']; ?>"><img src="<?php echo $row['avatar']; ?>" alt="<?php echo $row['title']; ?>" /></a>
                            <?php
                            if ($sale > 0) {
                                ?>
                                <div class="old-price">
                                    <del><?php echo $price; ?></del>  <?php echo $sale; ?>
                                </div>
                            <?php } else { ?>
                                <div class="old-price">
                                    <?php echo $price; ?>
                                </div>
                            <?php } ?>
                            <p>
                                <a href="<?php echo $link; ?>" title="<?php echo $row['title']; ?>" class="view-details" style="display: inline-block;">Xem chi tiết <i class="fa fa-angle-double-right"></i></a> 
                                <?php
                                if ($row['price'] > 0) {
                                    ?>
                                    <a href="mua-ngay/<?php echo $row['id']; ?>" style="display: inline-block;" class="buy-now">Mua ngay</a>
                                <?php } ?>
                            </p>
                            <?php
                            if ($sale > 0) {
                                ?>
                                <span class="sale-off">-<?php echo $percent; ?>%<br>Off</span>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            } else {
                echo "<center><em>Hiện sản phẩm đang cập nhật</em></center>";
            }
            ?>
        </div>
        <div class="page clearfix">
            <ul>
                <?php echo $this->pagination->create_links(); ?>
            </ul>
        </div>
        <script type="text/javascript">
            $('.item-in h2').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
            wow = new WOW({
                boxClass: 'wow', // default
                animateClass: 'animated', // default
                offset: 0, // default
                mobile: true, // default
                live: true // default
            })
            wow.init();
        </script>
    </div>
</section>