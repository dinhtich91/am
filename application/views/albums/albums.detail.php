<?php
$title = $detailAlbum['title'];
?>
<div class="page-container page-library-container group">
    <div class="block-library group">
        <ul class="lirary-header">
            <li class="active"><a href="thu-vien.html"><i class="fa fa-camera"></i> Album Ảnh</a></li>
            <li><a href="thu-vien/video.html"><i class="fa fa-video-camera"></i> Video Clip</a></li>
        </ul>
        <div class="lirary-content group">
            <div class="album-image group tab-library">
                <div class="album-image-2 group">
                    <h3><?= $title; ?></h3>
                    <div class="album-image-2-w">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <?php
                                foreach ($lists as $row) {
                                    ?>
                                    <div class="swiper-slide">
                                        <img src="upload/album/lagre/<?= $row['image']; ?>" alt="<?= $title; ?>">
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <?php
                                foreach ($lists as $row) {
                                    ?>
                                    <div class="swiper-slide">
                                        <img src="upload/album/thumb/<?= $row['image']; ?>" alt="<?= $title; ?>">
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main">
            <div class="wp_gallery clearfix">
                <div class="megafolio-container"></div> 
            </div> 
        </div>
    </div>
    <script>
        var galleryTop = new Swiper('.gallery-top', {
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 10
        });
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true
        });
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;

    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.clip').photobox('a', {time: 2500, autoplay: true});
        });
    </script>
</div>

