<!DOCTYPE html>
<html>
    <?php 
        $config = $this->myconfig->getConfig();
        $slider = $this->fromm->slider();
    ?>
    <head>
        <base href="<?php echo base_url(); ?>"/>
        <meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <title><?= isset($title_header) ? $title_header : $config['title_web']; ?></title>
        <meta content="index, follow" name="robots"/>
        <meta name="description" content="<?= isset($metaDescription) ? $metaDescription : $config['meta_description']; ?>" />
        <meta name="keywords" content="<?= (isset($metaKeyword) && $metaKeyword != "") ? $metaKeyword : $config['meta_keywords']; ?>" />
        <link rel="canonical" href="<?= current_url(); ?>" />
        <link rel="icon" href="../../favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">              

        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"> 
        <link type="text/css" rel="stylesheet" href="css/animate.min.css">
        <link type="text/css" rel="stylesheet" href="css/flexslider.css">        
        
        <link type="text/css" rel="stylesheet" href="js/video/showYtVideo.css">
        <link type="text/css" rel="stylesheet" href="js/facebox/facebox.css">
        <link type="text/css" rel="stylesheet" href="js/photobox/photobox.css">
        <link type="text/css" rel="stylesheet" href="css/custom.css"> 

        <script src="js/jquery-2.2.3.min.js"></script> 
        <script src="js/bootstrap.min.js"></script>
        
        <script src="js/video/jquery.showYtVideo.js"></script> 
        <script src="js/photobox/jquery.photobox.js"></script>    
        <script src="js/jquery.flexslider.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>

    </head>
    <body class="<?= isset($class_body) ? $class_body : "home"; ?>">

        <nav class="navbar navbar-inverse navbar-nhaam">
            <div class="container disable-padding">
                <div class="col-md-3-5 disable-padding">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="logo-box clearfix">
                            <a class="navbar-brand disable-padding-top" href="#"><img class="logo-nhaam" src="img/logo-nhaam.png" alt="logo-nhaam" title="logo-nhaam" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9-5 disable-padding">
                    <div id="navbar" class="collapse navbar-collapse disable-padding">
                        <ul class="nav navbar-nav navbar-mini-nhaam">
                            <li class="<?php echo $active_menu=='home'?'active':'' ?>"><a href="<?php  echo base_url('/'); ?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <li class="<?php echo $active_menu=='gioithieu'?'active':'' ?>"><a class="open-regular text-uppercase" href="<?php  echo site_url('gioi-thieu'); ?>"><?php echo $this->lang->line('gioithieu'); ?></a><span class="glyphicon glyphicon-triangle-bottom"></span></li>
                            <li class="<?php echo $active_menu=='dandung'?'active':'' ?>"><a class="open-regular text-uppercase" href="<?php  echo site_url('nha-am-dan-dung'); ?>"><?php echo $this->lang->line('dandung'); ?></a></li>
                            <li class="<?php echo $active_menu=='congnghiep'?'active':'' ?>"><a class="open-regular text-uppercase" href="<?php  echo site_url('nha-am-cong-nghiep'); ?>"><?php echo $this->lang->line('congnghiep'); ?></a></li>
                            <li class="<?php echo $active_menu=='maunha'?'active':'' ?>"><a class="open-regular text-uppercase" href="http://www.nhaam.com.vn/"><?php echo $this->lang->line('maunha'); ?></a></li>
                            <li class="<?php echo $active_menu=='thuvien'?'active':'' ?>"><a class="open-regular text-uppercase" href="<?php  echo site_url('thu-vien'); ?>"><?php echo $this->lang->line('thuvien'); ?></a></li>
                            <li class="<?php echo $active_menu=='tintuc'?'active':'' ?>"><a class="open-regular text-uppercase" href="<?php  echo site_url('tin-tuc'); ?>"><?php echo $this->lang->line('tintuc'); ?></a><span class="glyphicon glyphicon-triangle-bottom"></span></li>
                            <li class="<?php echo $active_menu=='lienhe'?'active':'' ?>"><a class="open-regular text-uppercase" href="<?php  echo site_url('lien-he'); ?>"><?php echo $this->lang->line('lienhe'); ?></a></li>
                            <li><a class="open-regular text-uppercase" href="<?php echo base_url('vn') ?>"><img src="img/vn-flag.png" alt="vn-flag" title="vn-flag" /></a></li>
                            <li><a class="open-regular text-uppercase" href="<?php echo base_url('en') ?>"><img src="img/usa-flag.png" alt="usa-flag" title="usa-flag" /></a></li>
                            <li><a class="open-regular text-uppercase" href="<?php echo base_url('de') ?>"><img src="img/jp-flag.png" alt="jp-flag" title="jp-flag" /></a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </nav>

        <div class="container-fluid disable-padding">
            <div class="home-slider">
                <ul class="slides">
                    <?php
                        if(isset($slider) && !empty($slider)){
                            foreach ($slider as $key => $value) {
                                ?>
                                    <li>
                                        <a href="<?php echo $value['link'] ?>">
                                            <img class="img-maxsize" src="<?php echo $value['image'] ?>" alt="<?php echo $value['title_'.$lang]; ?>" title="<?php echo $value['title_'.$lang]; ?>" />
                                        </a>
                                    </li>
                                <?php
                            }
                        }

                    ?>
                </ul>
                <div class="custom-navigation-2">
                    <div class="custom-controls-container-2"></div>
                </div>
                <div class="slogan-slider-box">
                    <div><p class="uvn-font slogan-text-slider"><?php echo $this->lang->line('nhaamtinhviet'); ?></p></div>
                </div>
            </div>
        </div>
        



