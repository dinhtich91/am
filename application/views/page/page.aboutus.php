
<div class="container-fluid disable-padding background-home">            
            <div class="container disable-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-content">
                            <ul class="nav nav-tabs navtab-nhaam">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li><a class="text-uppercase open-regular <?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>" href="<?php echo site_url('gioi-thieu/'.$value['tag']); ?>">
                                                 <?php echo $value['title_'.$lang]; ?>
                                                </a></li>
                                            <?php
                                        }
                                    }
                                ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-gioithieu">
                            <?php
                                if(isset($detail_gioithieu) && !empty($detail_gioithieu)){
                                    echo $detail_gioithieu['detail_'.$lang];
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            .content-gioithieu{
                background-color: #b7b7b7;
                padding: 20px 20px;
            }
        </style>