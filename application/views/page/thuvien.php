<div class="container-fluid disable-padding">
    <div class="fromm-product-details-info">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 disable-padding"><h3 class="contact-heading-background open-san-semibold">Tin tức hoạt động công ty</h3></div>
            </div>
            <div class="row" style="max-width:1170px; display: flex;">
                <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                    <ul class="nav nav-sidebar clearfix">
                        <li class="product-menu-background open-san-semibold clearfix">
                            <a class='disable-padding-top' href="#">Tin tức <span class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                    <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                        <li><a href="">Tin tức hoạt động công ty</a></li>
                        <li><a href="">Sự kiện</a></li>
                        <li><a href="">Cập nhật thông tin ngành</a></li>
                    </ul>                           
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px; ">
                    <div class="row disable-margin product-details-box" style="padding-bottom: 30px;">
                        <div style="max-width: 750px; margin: 0px auto; margin-top: 30px;">
                            <div id='gallery_temp'>
                               <div class="grid">
                                    <div class="grid-item grid-item--width1 grid-item--height1">
                                      <a href="img/bikes-ipad-1.png">
                                        <img src="img/bikes-ipad-1.png"
                                            title="photo1 title">
                                    </a>
                                    </div>
                                    <div class="grid-item grid-item--width2 grid-item--height2"></div>
                                    <div class="grid-item grid-item--width3 grid-item--height3"></div>
                                    <div class="grid-item grid-item--width4 grid-item--height3"></div>
                                    <div class="grid-item grid-item--width5 grid-item--height4"></div>
                                    <div class="grid-item grid-item--width6 grid-item--height4"></div>
                              </div>
                            </div>
                              
                            </div>
                        </div>                              
                    </div>                           
                </div>
            </div>
        </div>
    </div>            
</div> <!-- /container -->
<style type="text/css">
    .grid-item {
      width: 160px;
      height: 120px;
      float: left;
      background: #D26;
      border: 2px solid #fff;
      /*border-color: hsla(0, 0%, 0%, 0.5);
      border-radius: 5px;*/
    }

    .grid-item--width1{
        width: 305px;
    }
    .grid-item--width2{
        width: 440px;
    }
    .grid-item--width3{
        width: 127px;
    }
    .grid-item--width4{
        width: 313px;
    }
    .grid-item--width5{
        width: 432px;
    }
    .grid-item--width6{
        width: 314px;
    }

    .grid-item--height1 { 
        height: 315px; 
    }
    .grid-item--height2 { 
        height: 137px; 
    }
    .grid-item--height3 { 
        height: 178px; 
    }
    .grid-item--height4 { 
        height: 264px; 
    }



</style>
<script type="text/javascript">
    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: 1
    });
</script>
<script>

    // applying photobox on a `gallery` element which has lots of thumbnails links.
      // Passing options object as well:
      //-----------------------------------------------
      $('#gallery_temp').photobox('a',{ time:0 });

      // using a callback and a fancier selector
      //----------------------------------------------
      $('#gallery_temp').photobox('li > a.family',{ time:0 }, callback);
      function callback(){
         console.log('image has been loaded');
      }

      // // destroy the plugin on a certain gallery:
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('destroy');

      // // re-initialize the photbox DOM (does what Document ready does)
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('prepareDOM');
  </script>