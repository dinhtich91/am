<div class="container-fluid disable-padding background-home">
            <div class="container">
                
                    <div class="wrap-contact">
                    <form name="frm-contact" method="post" action="" id="frm-contact">
                        <div id="lang" value="<?php echo $lang; ?>"></div>
                        <div class="contact-title-top open-regular">
                            <p>Các yêu cầu, ý kiến đóng góp của quí vị luôn được chúng tôi ghi nhận và trả lời trong thời gian sớm nhất. Chúng tôi luôn sẵn sàng phục vụ quí vị với tinh thần làm việc và trách nhiệm cao nhất. Liên lạc với chúng tôi theo địa chỉ:</p>
                        </div>
                        <div class="contact-second-top clearfix">
                            <div class="col-md-9 contact-address-left">
                                <div class="contact-address">
                                    <div class="contact-title-address open-bold">
                                        <p>Nhà Ấm Trung tâm</p>
                                    </div>
                                    <div class="contact-detail-address open-regular">
                                        <p> 234B Khánh Hội, P.6, Q.4</p>
                                    </div>
                                </div>
                                <div class="contact-address">
                                    <div class="contact-title-address open-bold">
                                        <p>đông sài gòn</p>
                                    </div>
                                    <div class="contact-detail-address open-regular">
                                        <p> 112 Nguyễn Tuyển, P. Bình Trưng Tây, Q.2</p>
                                    </div>
                                </div>
                                <div class="contact-address">
                                    <div class="contact-title-address open-bold">
                                        <p>tây sài gòn</p>
                                    </div>
                                    <div class="contact-detail-address open-regular">
                                        <p> 17 Đường số 2, KDC Hương Lộ 5, P. An Lạc, Q. Bình Tân</p>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-3">
                                <div class="contact-detail-address open-regular">
                                    <p> ĐT: (84 8)  626 05 771</p>
                                </div>
                                <div class="contact-detail-address open-regular">
                                    <p> Email : congdoanam@nhaam.com</p>
                                </div>
                            </div>
                        </div>

                        <div class="contact-title-top open-regular" style="padding-top: 15px;">
                            <p>Hoặc liên hệ trực tiếp theo biểu mẫu dưới đây.</p>
                        </div>

                        <div class="contact-second-top clearfix">
                            <div class="col-md-5">
                                <div class="contact-input open-regular">
                                    <div class="title-contact-label">
                                        <label>Họ tên:</label>
                                    </div>
                                    <div class="title-contact-input">
                                        <input type="text" value="" name="hoten" />
                                    </div>
                                </div>
                                <div class="contact-input open-regular">
                                    <div class="title-contact-label">
                                        <label>Email:</label>
                                    </div>
                                    <div class="title-contact-input">
                                        <input type="text" value="" name="email" />
                                    </div>
                                </div>
                                <div class="contact-input open-regular">
                                    <div class="title-contact-label">
                                        <label>Điện thoại:</label>
                                    </div>
                                    <div class="title-contact-input">
                                        <input type="text" value="" name="dienthoai" />
                                    </div>
                                </div>
                                <div class="contact-input open-regular">
                                    <div class="title-contact-label">
                                        <label>Địa chỉ:</label>
                                    </div>
                                    <div class="title-contact-input">
                                        <input type="text" value="" name="diachi" />
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-7 right-contact">
                                <div class="contact-input open-regular">
                                    <div class="title-contact-label">
                                        <label>Địa chỉ công trình:</label>
                                    </div>
                                    <div class="title-contact-input">
                                        <input type="text" value="" name="diachicongtrinh" />
                                    </div>
                                </div>
                                <div class="contact-input open-regular clearfix">
                                    <div class="title-contact-label">
                                        <label>Nhu cầu:</label>
                                    </div>
                                    <div class="title-contact-input-checkbox">
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="checkbox" value="1" name="nhucau[]" /><span>Xây mới</span>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="checkbox" value="2" name="nhucau[]"  /><span>Sửa chữa</span>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="checkbox" value="3" name="nhucau[]"  /><span>Nội thất</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-input open-regular clearfix">
                                    <div class="title-contact-label">
                                        <label>Mục đích:</label>
                                    </div>
                                    <div class="title-contact-input-checkbox">
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="checkbox" value="1" name="mucdich[]"  /><span>Để ở</span>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="checkbox" value="2" name="mucdich[]" /><span>Cho thuê</span>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="checkbox" value="3" name="mucdich[]" /><span>Cả hai</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="contact-input open-regular clearfix contact-dientich">
                                    <div class="title-contact-label">
                                        <label style="padding: 0px;">Diện tích:</label>
                                    </div>
                                    <div class="title-contact-input-checkbox right-dientich">
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <label>Dài (m)</label><input type="input" value="" name="dai" />
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <label>Rộng (m)</label><input type="input" value="" name="rong" />
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <label>Tổng diện tích (m<sup>2</sup>)</label><input type="input" value="" name="tongdientich" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-input open-regular clearfix contact-dientich">
                                    <div class="title-contact-label">
                                        <label style="padding: 0px;">Quy mô:</label>
                                    </div>
                                    <div class="title-contact-input-checkbox right-dientich">
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="input" value="" name="tret" /><label>Trệt</label>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="input" value="" name="lung" /><label>Lửng</label>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="input" value="" name="lau" /><label>Lầu</label>
                                            </div>
                                        </div>
                                        <div class="checkbox-contact">
                                            <div class="color-checkbox">
                                                <input type="input" value="" name="santhuong" /><label>Sân thượng</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="contact-input-yeucau open-regular contact-textarea">
                                    <div class="title-contact-label" style="padding-right: 12px">
                                        <label>Yêu cầu khác:</label>
                                    </div>
                                    <div class="title-contact-input-text" style="display: block;">
                                        <textarea name="tinnhan"></textarea>
                                    </div>
                                </div>
                                <div class="btn-gui">
                                    <button type="submit">Gửi</button>
                                </div>
                            </div>
                        </div>

                        <div class="google-maps-box">
                            <div id="map"></div>
                        </div>
                    </form>
                    </div>
                
            </div>
        </div>

    <style type="text/css">
        .google-maps-box{
            padding: 0px 25px;
            min-height: 500px;
            width: 100%;
        }
        #map{
            width: 100%;
            height: 400px;
        }
        .btn-gui button{
            float: right;
            margin-right: 52px;
            padding: 2px 20px;
            background: red;
            border: 0px;
            color: #fff;
            margin-top: 5px;
        }
        .title-contact-input input{
            padding-left: 10px;
        }
    </style>
  
    <script>

      // This example displays a marker at the center of Australia.
      // When the user clicks the marker, an info window opens.

      function initMap() {
        var uluru = {lat: 10.759713, lng: 106.699390};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: uluru
        });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h3 id="firstHeading" class="firstHeading">Nhà Ấm Trung Tâm</h3>'+
            '<div id="bodyContent">'+
            '234B Khánh Hội, P.6, Q.4'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'Uluru (Ayers Rock)'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
        infowindow.open(map, marker);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrq9H_zkJigNkygBmGjDxStCbzZEyaOzM&callback=initMap">
    </script>


<script type="text/javascript">

    function checkValidate(){
    
    var $login_frm   = $('#frm-contact');
    var lang = $('#lang').attr('value');

    $login_frm.submit(function( event ) {
        var errAll      = true;
        if( !$login_frm.find('input[name="hoten"]').val() || $login_frm.find('input[name="hoten"]').val() === undefined ) {

            if(lang == 'vn'){
              alert('Tên không được để trống!');
            }
            else{
              alert('Name is required'); 
            }
            errAll = false;
        }

        else if( !$login_frm.find('input[name="email"]').val() || $login_frm.find('input[name="email"]').val() === undefined ) {

            if(lang == 'vn'){
              alert('Email không được để trống!');
            }
            else{
              alert('Email is required'); 
            }
            errAll = false;
        }

         else if( !$login_frm.find('input[name="diachi"]').val() || $login_frm.find('input[name="diachi"]').val() === undefined ) {

            if(lang == 'vn'){
              alert('Địa chỉ không được để trống!');
            }
            else{
              alert('Address is required'); 
            }
            errAll = false;
        }

        else if( !$login_frm.find('input[name="dienthoai"]').val() || $login_frm.find('input[name="dienthoai"]').val() === undefined || $.isNumeric( $('input[name="dienthoai"]').val()) == false ) {

            if(lang == 'vn'){
              alert('Điện thoại không hợp lệ!');
            }
            else{
              alert('Phone number is valid'); 
            }
            errAll = false;
        }
        else if( document.getElementById("file-tuyendung").files.length == 0 ){
            if(lang == 'vn'){
              alert('File không hợp lệ!');
            }
            else{
              alert('File is valid'); 
            }
            errAll = false;
        }
        

        return errAll;
    });

    $login_frm.find('.ipt').on('focus', function(e){
        e.preventDefault();
        $(this).siblings('.help-block').addClass('hidden');
    });
};
$(document).ready(function() {

    checkValidate();
});
  </script>