<script type="text/javascript" src="<?= base_url('access/js/ajaxupload.3.5.js'); ?>"></script>
<?php
$id_product = isset($detail['id']) ? $detail['id'] : 0;
$avatar = isset($detail['avatar']) && is_file($detail['avatar']) ? $detail['avatar'] : ICON_UPLOAD_PHOTO;
$title = isset($detail['title']) ? stripslashes($detail['title']) : "";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
$status = isset($detail['status']) ? $detail['status'] : 1;
$category = isset($detail['cat_id']) ? $detail['cat_id'] : "";
$type = isset($detail['type']) ? $detail['type'] : 0;
$overview = isset($detail['overview']) ? $detail['overview'] : "";
$price = isset($detail['price']) ? $detail['price'] : 0;
$sale = isset($detail['sale']) ? $detail['sale'] : "";
$content = isset($detail['detail']) ? stripslashes($detail['detail']) : "";
$notes = isset($detail['notes']) ? stripslashes($detail['notes']) : "";
$uses = isset($detail['detail']) ? stripslashes($detail['uses']) : "";
$is_popup = isset($detail['is_popup']) ? $detail['is_popup'] : 1;
$is_phukien = isset($detail['is_phukien']) ? $detail['is_phukien'] : 0;
$thanhphan = isset($detail['thanhphan']) ? stripslashes($detail['thanhphan']) : "";
$link_album = isset($detail['link_album']) ? stripslashes($detail['link_album']) : "";
?>
<style>
    .item-photo{float: left;margin: 5px;border: 1px solid #ccc;padding: 5px;text-align: center;}
</style>
<div id="content">
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">   
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">
                            <tr>
                                <td>Tên sản phẩm:
                                    <span class="required">*</span>                                    
                                </td>
                                <td><input type="text" name="title" value="<?= $title; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Giá gốc:
                                    <span class="required">*</span>                                    
                                </td>
                                <td>
                                    <input style="width: 100px;" type="text" name="price" value="<?= $price; ?>"/><br/><br/>
                                    <strong>
                                        <em>
                                            Nhập 0: nếu là Liên hệ.
                                            <br/>Giá là các chữ số: ví dụ: 500.000 thì nhập 500000
                                        </em>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Giá bán:
                                    <span class="required">*</span>                                    
                                </td>
                                <td>
                                    <input style="width: 100px;" type="text" name="sale" value="<?= $sale; ?>"/><br/><br/>
                                    <strong>
                                        <em>Nếu không có giá khuyến mãi thì để trống.</em>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Danh mục:</td>
                                <td>
                                    <select name="cat_id">        
                                        <option value="0">--None--</option> 
                                        <?php
                                        foreach ($dequycategory as $k => $row) {
                                            ?>                                     
                                            <option value="<?= $row['id'] ?>" <?= $category == $row['id'] ? "selected='selected'" : "" ?>><?= $row['title'] ?></option> 
                                        <?php } ?>  
                                    </select>   
                                </td>
                            </tr>
                            <tr>
                                <td>Hình đại diện: (650x500px)</td>
                                <td>
                                    <input type="hidden" name="avatar" value="<?= $avatar; ?>" size="70" id="hiddenLogo">
                                    <div id="uploadLogo"><img src="<?= $avatar; ?>" id="img_logo" style="max-width: 100px;"/></div>
                                    <span id="loadAjax"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Hình phụ: (650x500px)</td>
                                <td>
                                    <p><a onclick="admin.addPhoto();">Thêm hình</a></p>
                                    <?php
                                    $listThumb = $this->products->getImgThumb($id_product);
                                    foreach ($listThumb as $row) {
                                        ?>
                                        <div class="item-photo" id="id_<?= $row['id']; ?>">
                                            <input type="hidden" id="imageHidden_id_<?= $row['id']; ?>" value="upload/images/<?= $row['image']; ?>" name="images[]">
                                            <div id="upload_id_<?= $row['id']; ?>" class=""><img height="60" src="upload/images/<?= $row['image']; ?>"></div><span id="status_1433818501143"></span>
                                            <div><a onclick="admin.removePhoto('id_<?= $row['id']; ?>');" href="javascript:void(0);"><img src="access/image/b_drop.png"></a></div>
                                        </div>
                                    <?php } ?>
                                    <div id="addPhoto"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>Liên kết đến album:</td>
                                <td>
                                    <input type="text" name="link_album" value="<?= $link_album; ?>"/>
                                    (Nhập link Album muốn liên kết)
                                </td>
                            </tr>
                            <tr>
                                <td>Mô tả ngắn: </td>
                                <td><?= $this->function->display_CKEditor("overview", $overview, 300); ?></td>
                            </tr>
                            <tr>
                                <td>Thông tin sản phẩm: </td>
                                <td><?= $this->function->display_CKEditor("detail", $content, 300); ?></td>
                            </tr>
                            <tr>
                                <td>Thanh toán: </td>
                                <td><?= $this->function->display_CKEditor("thanhphan", $thanhphan, 300); ?></td>
                            </tr>
                            <tr>
                                <td>Điều khoản mua hàng: </td>
                                <td><?= $this->function->display_CKEditor("notes", $notes, 300); ?></td>
                            </tr>
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Trạng thái:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Hiển thị </label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Ẩn</label>
                                </td>
                            </tr>  
                        </table>
                    </div>
                </div>              
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
        admin.uploadLogo();
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>