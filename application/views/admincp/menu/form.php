<?php 
$v_title         = isset($detail['v_title'])?$detail['v_title']:"";
$v_tag           = isset($detail['v_tag'])?$detail['v_tag']:"";
$link_front_page = isset($detail['link_front_page'])?$detail['link_front_page']:"";
$link_back_end   = isset($detail['link_back_end'])?$detail['link_back_end']:"";
$ordering        = isset($detail['ordering'])?$detail['ordering']:$orderingMax;
$type            = isset($detail['type'])?$detail['type']:"";
$status          = isset($detail['status'])?$detail['status']:"1";
$is_front_page   = isset($detail['is_front_page'])?$detail['is_front_page']:"1";
$parent          = isset($detail['parent'])?$detail['parent']:"0";
?>
<div id="content">
    <div class="breadcrumb">
        <br />        
    </div>
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">               
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">                
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tên menu:</td>
                                <td><input name="v_title" value="<?=$v_title;?>" size="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>Danh mục cha:</td>
                                <td>
                                    <select name="parent" >        
                                        <option value="0">--None--</option> 
                                        <?php                                 
                                        foreach($dequyMenu as $k=>$row){                                            
                                        ?>                                     
                                       <option value="<?=$row['id']?>" <?=$parent==$row['id']?"selected='selected'":""?>><?=$row['v_title']?></option> 
                                        <?php }?>  
                                    </select>   
                                </td>
                            </tr>
                           <tr>
                                <td>Link Front Page:</td>
                                <td><input name="link_front_page" value="<?=$link_front_page;?>" size="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>Link Back End:</td>
                                <td><input name="link_back_end" value="<?=$link_back_end;?>" size="50" />
                                </td>
                            </tr>                                                  

                            <tr>
                                <td> Type:</td>
                                <td><input name="type" value="<?=$type;?>" size="10" />
                                </td>
                            </tr>
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?=$ordering;?>" size="1" />
                                </td>
                            </tr>      
                            <tr>
                                <td> Hiển thị ở trang ngoài:</td>
                                <td>
                                    <label><input type="radio" value="1" name="is_front_page" <?=$is_front_page==1?"checked='checked'":""?> /> Có </label>
                                    <label><input type="radio" value="0" name="is_front_page" <?=$is_front_page==0?"checked='checked'":""?>/> Không</label>
                                </td>
                            </tr> 
                            <tr>
                                <td> Trạng thái:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?=$status==1?"checked='checked'":""?> /> Hiển thị </label>
                                    <label><input type="radio" value="0" name="status" <?=$status==0?"checked='checked'":""?>/> Ẩn</label>
                                </td>
                            </tr>                          
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a'); 
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
