<title>Chi tiết đơn hàng</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
    table{border-top: 1px solid #ccc;border-right: 1px solid #ccc;width: 100%;margin-bottom: 10px;}
    table tr td{border-bottom: 1px solid #ccc;border-left: 1px solid #ccc;padding: 4px;}
</style>
<table border="0" cellpadding="0" cellspacing="1" >
    <tbody>
        <tr>
            <td style="text-align:left"><strong>Mã đơn hàng: </strong> </td>
            <td style="text-align:right"><strong>Thời gian mua hàng</strong></td>
            <td style="text-align:right"><strong>Họ tên: </strong></td>
            <td style="text-align:right"><strong>Địa chỉ: </strong></td>
            <td style="text-align:right"><strong>Điện thoại: </strong></td>
        </tr>
        <tr>
            <td><?php echo $detail['id']; ?></td>
            <td style="text-align:right"><?php echo date("d/m/Y H:i:s", strtotime($detail['datetime'])); ?>&nbsp;</td>
            <td style="text-align:right"><?php echo $detail['name']; ?></td>
            <td style="text-align:right"><?php echo $detail['address']; ?></td>
            <td style="text-align:right"><?php echo $detail['phone']; ?></td>
        </tr>
        <?php
        ?>
        
    </tbody>
</table>




<table border="0" cellpadding="0" cellspacing="1" style="widtd: 100%">
    <tbody>
        <tr>            
            <td style="text-align: left;">
                Tên sản phẩm</td>
            <td style="text-align: right;">
                Số lượng</td>
            <td style="text-align: right;">
                Chi tiết</td>
        </tr>
        <?php
            $name = $detail['name'];
            $quanty = $detail['quality'];
            $link = $detail['link_product'];
        ?>
        <tr>
            <td>
                <strong><?php echo $name; ?></strong>
            </td>
            <td style="text-align: right;">
                <strong><?php echo $quanty; ?></strong>
            </td>
            <td style="text-align: right;">
                <strong><a href="<?php echo $link; ?>" target="_blank"><?php echo $link; ?></a></strong>
            </td>          
        </tr>
    </tbody>
</table>