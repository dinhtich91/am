<form action="" method="post" enctype="multipart/form-data" class="forms" name="form" id="frm">
    <div class="page-title ui-widget-content ui-corner-all">
        <div class="float-left">
            <h1><b><?php echo $title_header; ?></b></h1> 
        </div>

        <div class="button float-right">        
            <a href="javascript:submit();" class="btn ui-state-default"><span class="ui-icon ui-icon-circle-plus"></span>Lưu lại</a>
        </div>
        <div class="clearfix"></div>
        <div class="other">
            <div class="error"><?php echo form_error('title') ?></div>  
            <div class="hastable">                  
                <div style="float: left; width:99%; padding-left:1%;">    
                    <div id="thongtin" class="show1">
                        <ul>                                                       
                            <li>
                                <label class="desc">Thông tin</label>
                                <div>
                                <?php
                                $this->function->display_FCKEditor('detail', stripslashes($detail));
                                ?>
                                </div>    
                            </li>                                
                        </ul>
                    </div>

                </div>  
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</form>