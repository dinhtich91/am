<?
$i = 0;
if(count($volume_price)>0){
foreach ($volume_price as $row) {
    $id       = $row['id'];
    $volume   = $row['volume'];
    $price    = $row['price'];
    $sale     = $row['sale']==0?"":$row['sale'];
    $format_p = number_format($price, 0, '.', '.');
    $format_s = $row['sale']==0?"":number_format($sale, 0, '.', '.');
?>

    <? if ($i == 0) { ?>

        <tr>
            <td>Thể tích / Giá gốc / Sale:
                <span class="required">*</span>                                    
            </td>
            <td>
                <span class="volume">
                    <a onclick="addVP();"><img src="<?= ICON_ADD_NEWS; ?>"/></a>
                    <input name="volume[]" value="<?=$volume;?>" size="10"/>(ml) / 
                </span>
                
                <span class="price">
                    <input name="price[]" value="<?= $price; ?>" size="10" onkeyup="changePriceText(this,'text-default<?= $id; ?>', this.value)"/>
                    <span style="font-weight: bold;" id="text-default<?= $id; ?>"><?=$format_p;?></span> VND
                </span>
                
                <span class="sale">
                    <input name="sale[]" value="<?= $sale; ?>" size="10" onkeyup="changePriceText(this,'sale<?= $id; ?>', this.value)"/>
                    <span style="font-weight: bold;" id="sale<?= $id; ?>"><?=$format_s;?></span> VND
                </span>
                
            </td>
        </tr>

    <? } else { ?>
        <tr id="row-<?= $id; ?>">
            <td>&nbsp;</td>
            <td>
                <span class="volume">
                    <a onclick="removeVP('<?= $id; ?>');"><img src="<?= ICON_DEL; ?>"/></a>
                    <input name="volume[]" value="<?= $volume; ?>" size="10"/>(ml) /         
                </span>
                       
                <span class="price">
                    <input name="price[]" value="<?= $price; ?>" size="10" onkeyup="changePriceText(this,'text<?= $id; ?>', this.value)"/>
                    <span style="font-weight: bold;" id="text<?= $id; ?>"><?=$format_p;?></span> VND
                </span>
                
                <span class="sale">
                    <input name="sale[]" value="<?= $sale; ?>" size="10" onkeyup="changePriceText(this,'sale<?= $id; ?>', this.value);"/>
                    <span style="font-weight: bold;" id="sale<?= $id; ?>"><?=$format_s;?></span> VND
                </span>
                
            </td>
        </tr>
    <? } ?>
<? $i++;}}else{?>
     <tr>
            <td>Thể tích / Giá gốc / Sale:
                <span class="required">*</span>                                    
            </td>
            <td>
                
                <span class="volume">
                    <a onclick="addVP();"><img src="<?= ICON_ADD_NEWS; ?>"/></a>
                    <input name="volume[]" value="0" size="10"/>(ml) / 
                </span>
                
                <span class="price">
                    <input name="price[]" value="0" size="10" onkeyup="changePriceText(this,'text-default', this.value);"/>
                    <span style="font-weight: bold;" id="text-default"></span> VND  / 
                </span>
                
                <span class="sale">
                    <input name="sale[]" value="0" size="10" onkeyup="changePriceText(this,'sale', this.value);"/>
                    <span style="font-weight: bold;" id="sale"></span> VND
                </span>
            </td>
        </tr>   
<? } ?>
