<script type="text/javascript" src="<?= base_url('access/js/ajaxupload.3.5.js'); ?>"></script>
<?php
$title = isset($detail['title']) ? $detail['title'] : "";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
$desc = isset($detail['desc']) ? $detail['desc'] : "";
$banner = isset($detail['banner']) && is_file($detail['banner']) ? $detail['banner'] : ICON_UPLOAD_PHOTO;
$status = isset($detail['status']) ? $detail['status'] : "1";
$is_menu = isset($detail['is_menu']) ? $detail['is_menu'] : "1";
$parent = isset($detail['parent']) ? $detail['parent'] : 0;
$is_title = isset($detail['is_title']) ? $detail['is_title'] : 0;
$is_desc = isset($detail['is_desc']) ? $detail['is_desc'] : 0;
?>
<div id="content">
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">               
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">                
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tên danh mục:</td>
                                <td><input name="title" value="<?= $title; ?>" size="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>Danh mục cha:</td>
                                <td>
                                    <select name="parent">        
                                        <option value="0">--None--</option> 
                                        <?php
                                        foreach ($dequycategory as $k => $row) {
                                            ?>                                     
                                            <option value="<?= $row['id'] ?>" <?= $parent == $row['id'] ? "selected='selected'" : "" ?>><?= $row['title'] ?></option> 
                                        <?php } ?>  
                                    </select>   
                                </td>
                            </tr>
                            <tr>
                                <td>Mô tả:</td>
                                <td>
                                    <textarea name="desc"><?= stripslashes($desc); ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Hình banner:</td>
                                <td>
                                    <input type="hidden" name="banner" value="<?= $banner; ?>" size="70" id="imageHidden_banner">
                                    <div id="upload_banner"><img src="<?= $banner; ?>" id="img_logo" style="max-width: 100px;"/></div>
                                    <span id="status_banner"></span>
                                    <br/><strong>(Size: 1600x570px)</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Hiện menu:</td>
                                <td>
                                    <label><input type="radio" value="1" name="is_menu" <?= $is_menu == 1 ? "checked='checked'" : "" ?> /> Có</label>
                                    <label><input type="radio" value="0" name="is_menu" <?= $is_menu == 0 ? "checked='checked'" : "" ?>/> Không</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Hiện tiêu đề:</td>
                                <td>
                                    <label><input type="radio" value="1" name="is_title" <?= $is_title == 1 ? "checked='checked'" : "" ?> /> Có</label>
                                    <label><input type="radio" value="0" name="is_title" <?= $is_title == 0 ? "checked='checked'" : "" ?>/> Không</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Hiện mô tả:</td>
                                <td>
                                    <label><input type="radio" value="1" name="is_desc" <?= $is_desc == 1 ? "checked='checked'" : "" ?> /> Có</label>
                                    <label><input type="radio" value="0" name="is_desc" <?= $is_desc == 0 ? "checked='checked'" : "" ?>/> Không</label>
                                </td>
                            </tr>
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>     
                            <tr>
                                <td> Trạng thái:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Hiển thị </label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Ẩn</label>
                                </td>
                            </tr>                          
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        admin.uploadPhoto("banner");
    });
</script>
