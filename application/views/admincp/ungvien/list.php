<div id="content">
    <div class="breadcrumb">
        <br />

    </div>
    <div class="box">
        <div class="left"></div>
        <div></div>
        <div class="heading">
            <h1 style="background-image: url('access/image/review.png');">
                <a href="<?= $task_list; ?>"><?= $title_header; ?> (Tổng số: <?= $total_rows; ?>)</a></h1>
            <div class="buttons" style="float:right;">
                <!--<button onclick="return Question_Update();">Cập nhật vị trí</button>-->
                <button onclick="return Question_Delete_All();">Xóa chọn</button>                
                <!--<a href="<?= $task_add; ?>"><button>Thêm</button></a>-->
            </div>
        </div>
        <div class="content">
            <?php
            $messages = $this->messages->get();
            if (is_array($messages)):
                foreach ($messages as $type => $msgs):
                    if (count($msgs > 0)):
                        foreach ($msgs as $message):
                            echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                        endforeach;
                    endif;
                endforeach;
            endif;
            ?>   
            <form name="LISTFORM" id="LISTFORM" method="post" action="<?= $action_form; ?>" enctype="multipart/form-data">                
                <table class="list">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
                                <input onclick="javascript: selectAll(this.checked);" type="checkbox" name="checkall"/>
                            </td>
                            <td width="110">Vào lúc</td>
                            <td width="10%">Vị trí ứng tuyển</td>
                            <td width="10%">Họ tên</td>
                            <td width="15%">Email</td>                                                 
                            <td width="10%">Số điện thoại</td>
                            <td>Địa chỉ</td>

                            <td>CV Ứng viên</td>
                            <td  width="150">Xóa</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list as $row) {
                            $vitrituyen = $this->ungviens->getViTri($row['vitri_tuyen']);
                            $id = $row['id'];
                            $name = $row['hoten'];
                            $email = $row['email'];
                            $phone = $row['dienthoai'];
                            $name_product = $row['cv'];
                            $address = $row['diachi'];
                            $add_date = date("d-m-Y | H:i", strtotime($row['add_date']));
                            ?>
                            <!--###Lặp đoạn này###-->
                            <tr>
                                <td style="text-align: center;"><input  type="checkbox" name="del[]" value="<?= $id; ?>"> </td>
                                <td><?= $add_date; ?></td>
                                <td><strong><?= $vitrituyen[0]['title_vn']; ?></strong></td> 
                                <td><strong><?= $name; ?></strong></td>			            
                                <td><?= $email; ?></td>                                
                                <td><?= $phone; ?></td>
                                <td><?= $address; ?></td>
                                <td><strong><a href="<?= $name_product; ?>" target="_blank"><?= $name_product; ?></a></strong></td>
                                
                                <td><a onclick="Question_Delete('<?= $task_del; ?>/<?= $id; ?>');">Xóa</a></td>
                            </tr> 
                            <!--###Lặp đoạn này###-->
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script type="text/javascript">
                                function changeStatus(this_, id, field) {
                                    var status = $(this_).attr("status");
                                    $.ajax({
                                        url: '<?= $task_status; ?>/' + id + '/' + status + '/' + field,
                                        type: "get",
                                        beforeSend: function () {
                                            $(this_).attr("src", '<?= IMG_LOADING; ?>');
                                        },
                                        success: function (data) {
                                            $(this_).attr("status", data)
                                            if (data == 1)
                                                $(this_).attr("src", '<?= STATUS_1; ?>');
                                            else if (data == 0)
                                                $(this_).attr("src", '<?= STATUS_0; ?>');

                                        }
                                    });
                                }
</script>
