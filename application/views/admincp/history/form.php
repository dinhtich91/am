<?php
$year = isset($detail['year']) ? stripslashes($detail['year']) : "";
$desc = isset($detail['desc']) ? stripslashes($detail['desc']) : "";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
$status = isset($detail['status']) ? $detail['status'] : 1;
?>
<div id="content">
    <div class="breadcrumb">
        <br />        
    </div>
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Save" class="button_v1">   
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Cancel" class="button_v1">
                </div>
            </div>
            <div class="content">
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>                   
                <!--####-->
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">
                            <tr>
                                <td>Năm:</td>
                                <td><input name="year" value="<?= $year; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Nội dung: </td>
                                <td><?= $this->function->display_CKEditor("desc", stripslashes($desc), 250); ?></td>
                            <tr>
                                <td>Vị trí:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>                           
                            <tr>
                                <td>Tình trạng:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> />Show</label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/>Hide</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script>
        var num = <?= $i; ?>;
        function addtion() {
            num++;
            var html = "";
            html += '<tr id="row-img-' + num + '">';
            html += '<td>';
            html += 'Hình ' + num;
            html += '<div class="comments"></div>';
            html += '</td>';
            html += '<td>';
            html += '<input name="default_image[' + num + ']" value="" size="50" id="default_image' + num + '">';
            html += '<img src="<?= ICON_UPLOAD; ?>"  style="vertical-align: middle; cursor: pointer;" onclick="openKCFinder(\'default_image' + num + '\',\'images\',\'<?= trim_slashes(PATH_SITE . "/" . FOLDER_UPLOAD); ?>\',true)" title="Chọn hình"/>  &nbsp;';
            html += '<input name="hover_image[' + num + ']" value="" size="50" id="hover_image' + num + '">';
            html += '<img src="<?= ICON_UPLOAD; ?>"  style="vertical-align: middle; cursor: pointer;" onclick="openKCFinder(\'hover_image' + num + '\',\'images\',\'<?= trim_slashes(PATH_SITE . "/" . FOLDER_UPLOAD); ?>\',true)" title="Chọn hình"/>  &nbsp;';
            html += '</td>';
            html += '</tr>';
            $(".footer-img").before(html);
        }
        function remove(num) {
            $("#row-img-" + num).remove();
        }
</script>



