<?php
$title_vn           = isset($detail['title_vn']) ? stripslashes($detail['title_vn']) : "";
$title_en           = isset($detail['title_en']) ? stripslashes($detail['title_en']) : "";
$title_de           = isset($detail['title_de']) ? stripslashes($detail['title_de']) : "";

$footer1_vn           = isset($detail['footer1_vn']) ? stripslashes($detail['footer1_vn']) : "";
$footer1_en           = isset($detail['footer1_en']) ? stripslashes($detail['footer1_en']) : "";
$footer1_de           = isset($detail['footer1_de']) ? stripslashes($detail['footer1_de']) : "";

$footer2_vn           = isset($detail['footer2_vn']) ? stripslashes($detail['footer2_vn']) : "";
$footer2_en           = isset($detail['footer2_en']) ? stripslashes($detail['footer2_en']) : "";
$footer2_de           = isset($detail['footer2_de']) ? stripslashes($detail['footer2_de']) : "";

$footer3_vn           = isset($detail['footer3_vn']) ? stripslashes($detail['footer3_vn']) : "";
$footer3_en           = isset($detail['footer3_en']) ? stripslashes($detail['footer3_en']) : "";
$footer3_de           = isset($detail['footer3_de']) ? stripslashes($detail['footer3_de']) : "";
//$image              = isset($detail['image']) ? $detail['image'] : "";
?>
<div id="content">
    <div class="breadcrumb">
        <br />        
    </div>
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">   
                    <!--<input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">-->
                </div>
            </div>
            <div class="content">
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>                  
                <!--####-->
                <div id="tabs" class="htabs">
                    <a tab="#tab_general_vn"><?= IMG_VN ?></a>
                    <a tab="#tab_general_en"><?= IMG_EN ?></a>
                    <a tab="#tab_general_de"><?= IMG_DE ?></a>
                </div>

                <div id="tab_general_vn">                   
                    <div id="language1">
                        <table class="form">
                            <!--<tr>
                                <td>
                                    Hình ảnh
                                    <div class="comments">Kích thước hình : 410x255</div>
                                </td>
                                <td>
                                    <input name="image" value="<?= $image; ?>" size="70"id="image">
                                    <img src="<?=ICON_UPLOAD;?>"  style="vertical-align: middle; cursor: pointer;" onclick="openKCFinder('image','images','')"/>
                                </td>
                            </tr>-->
                            <!--<tr>                             
                                <td colspan="2"></td>
                            </tr>-->
                            <tr>
                                <td>Footer 1: </td>
                                <td><?= $this->function->display_CKEditor("title_vn", $title_vn, 250); ?></td>
                            </tr>
                            <tr>
                                <td>Footer 2: </td>
                                <td><?= $this->function->display_CKEditor("footer1_vn", $footer1_vn, 250); ?></td>
                            </tr>
                            <tr>
                                <td>Footer 3: </td>
                                <td><?= $this->function->display_CKEditor("footer2_vn", $footer2_vn, 250); ?></td>
                            </tr> 
                            <tr>
                                <td>Footer 4: </td>
                                <td><?= $this->function->display_CKEditor("footer3_vn", $footer3_vn, 250); ?></td>
                            </tr> 
                        </table>
                    </div>
                </div>

                <div id="tab_general_en">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Footer 1: </td>
                                <td><?= $this->function->display_CKEditor("title_en", $title_en, 250); ?></td>
                            </tr>
                            <tr>
                                <td>Footer 2: </td>
                                <td><?= $this->function->display_CKEditor("footer1_en", $footer1_en, 250); ?></td>
                            </tr>
                            <tr>
                                <td>Footer 3: </td>
                                <td><?= $this->function->display_CKEditor("footer2_en", $footer2_en, 250); ?></td>
                            </tr> 
                            <tr>
                                <td>Footer 4: </td>
                                <td><?= $this->function->display_CKEditor("footer3_en", $footer3_en, 250); ?></td>
                            </tr>                      
                        </table>
                    </div>
                </div>

                <div id="tab_general_de">                   
                    <div id="language1">
                        <table class="form">   
                            <tr>
                                <td>Footer 1: </td>
                                <td><?= $this->function->display_CKEditor("title_de", $title_de, 250); ?></td>
                            </tr>
                            <tr>
                                <td>Footer 2: </td>
                                <td><?= $this->function->display_CKEditor("footer1_de", $footer1_de, 250); ?></td>
                            </tr>
                            <tr>
                                <td>Footer 3: </td>
                                <td><?= $this->function->display_CKEditor("footer2_de", $footer2_de, 250); ?></td>
                            </tr> 
                            <tr>
                                <td>Footer 4: </td>
                                <td><?= $this->function->display_CKEditor("footer3_de", $footer3_de, 250); ?></td>
                            </tr>                      
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a'); 
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
