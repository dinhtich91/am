<?php
$title = isset($detail['title']) ? $detail['title'] : "";
$image = isset($detail['image']) ? $detail['image'] : "";
$link = isset($detail['link']) ? $detail['link'] : "";
$status = isset($detail['status']) ? $detail['status'] : "1";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
?>
<script>
    var DIR_FOLDER = "<?= base_url(); ?>";
    var width = 800;
    var height = 400;
    var iLeft = (screen.width - width) / 2;
    var iTop = (screen.height - height) / 2;
    function openKCFinder(field, type, path_upload, add) {

        window.KCFinder = {
            callBack: function(url) {
                if (path_upload && path_upload != "") {
                    var urlArray = url.split(path_upload);   // cắt bỏ đường dẫn thư mục gốc chứa hình ảnh
                    url = urlArray[1];
                }
                $("#" + field).val(url);
                window.KCFinder = null;
                if (add)
                    addtion();

            }
        };
        window.open(DIR_FOLDER + 'editor/kcfinder/browse.php?type=' + type + '&dir=' + type + '/public', 'kcfinder_textbox',
                'status=0, toolbar=0, location=0, menubar=0, directories=0,resizable=1, scrollbars=0, width=' + width + ', height=' + height + ',top=' + iTop + ",left=" + iLeft);
    }
</script>
<div id="content">
    <div class="breadcrumb">
        <br />        
    </div>
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">               
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">                
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tiêu đề</td>
                                <td><input name="title" value="<?= $title; ?>" size="50" />
                                </td>
                            </tr>  
<!--                            <tr>
                                <td>
                                    Hình ảnh đại diện
                                    <div class="comments"></div>
                                </td>
                                <td>
                                    <input name="image" value="<?= $image; ?>" size="70"id="image">
                                    <img src="<?= ICON_UPLOAD; ?>"  style="vertical-align: middle; cursor: pointer;" onclick="openKCFinder('image', 'images', '<?= trim_slashes(PATH_SITE . "/" . FOLDER_UPLOAD); ?>', false)" title="Chọn hình"/>                                
                                </td>
                            </tr>-->
                            <tr>
                                <td>
                                    Link YouTube
                                    <div class="comments">https://www.youtube.com/watch?v=Oj091SQKN4I</div>
                                </td>
                                <td><input name="link" value="<?= $link; ?>" size="70" />
                                </td>
                            </tr> 
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>     
                            <tr>
                                <td> Trạng thái:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Hiển thị </label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Ẩn</label>
                                </td>
                            </tr>                          
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
    $.tabs('#tabs a');
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
