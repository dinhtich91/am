<div class="container-fluid disable-padding background-home">            
            <div class="container disable-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-content">
                            <ul class="nav nav-tabs navtab-nhaam">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li>
                                                    <a class="text-uppercase open-regular a-1 <?php
                                                        if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                            echo 'active';
                                                        }
                                                     ?>" href="<?php echo site_url('tin-tuc/'.$value['tag']); ?>">
                                                     <?php echo $value['title_'.$lang]; ?>
                                                    </a>
                                                    <?php
                                                        if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                            ?>
                                                            <ul class="sub-menu-tintuc">
                                                                <?php
                                                                    $child_thuvien = $this->fromm->get_child_tintuc($value['id']);
                                                                    if(!empty($child_thuvien)){
                                                                        foreach ($child_thuvien as $k => $v) {
                                                                            ?>
                                                                                <li>
                                                                                    <a href="<?php echo site_url('tin-tuc/'.$value['tag'].'/'.$v['tag']); ?>"><?php echo $v['title_'.$lang]; ?></a>
                                                                                </li>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </ul>
                                                            <?php
                                                        }
                                                    ?>
                                                </li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>

                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row row-tintuc">
                                        <?php
                                            if(isset($list_new_cate) && !empty($list_new_cate)){
                                                foreach ($list_new_cate as $key => $value) {
                                                    ?>
                                                        <div class="col-md-6 margin-tottom">
                                                            <div class="tin-tuc-block clearfix">
                                                                <div class="col-md-4 disable-padding">
                                                                    <img src="img/tt1.png"/>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <h3 class="disable-margin-top open-bold tintuc-title"><?php echo $value['title_'.$lang]; ?></h3>
                                                                    <p class="open-regular white-text"><?php echo $value['title_'.$lang]; ?></p>
                                                                    <a href="<?php echo base_url('tin-chi-tiet/'.$value['tag']); ?>">
                                                                        <button class="read-more-btn open-regular read-more-tt"><?php echo $this->lang->line('xemchitiet'); ?></button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                        ?>
                                        
                                        
                                    </div>
                                </div>
                                <div class="phan-trang">
                                    <ul class="pagination">
                                      <?php echo $this->pagination->create_links(); ?>
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>