<div class="container-fluid disable-padding background-home">            
            <div class="container disable-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-content">
                            <ul class="nav nav-tabs navtab-nhaam">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li>
                                                    <a class="text-uppercase open-regular a-1 <?php
                                                        if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                            echo 'active';
                                                        }
                                                     ?>" href="<?php echo site_url('tin-tuc/'.$value['tag']); ?>">
                                                     <?php echo $value['title_'.$lang]; ?>
                                                    </a>
                                                    <?php
                                                        if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                            ?>
                                                            <ul class="sub-menu-tintuc">
                                                                <?php
                                                                    $child_thuvien = $this->fromm->get_child_tintuc($value['id']);
                                                                    if(!empty($child_thuvien)){
                                                                        foreach ($child_thuvien as $k => $v) {
                                                                            ?>
                                                                                <li>
                                                                                    <a href="<?php echo site_url('tin-tuc/'.$value['tag'].'/'.$v['tag']); ?>"><?php echo $v['title_'.$lang]; ?></a>
                                                                                </li>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </ul>
                                                            <?php
                                                        }
                                                    ?>
                                                </li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>

                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row row-tintuc">
                                        <h3 class="open-semi tintuc-headding"><?php echo $list_new_cate['title_'.$lang] ?></h3>
                                        <div id="gallery_temp">
                                            <?php
                                            if(isset($list_image) && !empty($list_image)){
                                                foreach ($list_image as $key => $value) {
                                                    ?>
                                                        <div class="col-md-4 disable-padding-left margin-tottom">
                                                            <div class="chi-tiet-box">
                                                                <a href="upload/album/lagre/<?php echo $value['image']; ?>">
                                                                    <img class="thumb-nail-img" src="upload/album/thumb/<?php echo $value['image']; ?>" />
                                                                </a>
                                                            </div>
                                                        </div> 
                                                    <?php
                                                }
                                            }
                                        ?>
                                        </div>    
                                        
                                        <div class="col-md-12 border-chitiet-tintuc"></div>
                                    </div>
                                    <div class="row row-tintuc-2">
                                        <div class="tintuclienquan clearfix">
                                            <h3 class="cleafix text-uppercase open-semi"><?php echo $this->lang->line('tinlienquan'); ?></h3>
                                            <div class="chi-tiet-lienquan clearfix">
                                                <ul class="slides">
                                                <?php
                                                    if(isset($tinlienquan) && !empty($tinlienquan)){
                                                        foreach ($tinlienquan as $key => $value) {
                                                            ?>
                                                                <li class="related-li">
                                                                    <div class="col-md-12 disable-padding related-slider">
                                                                        <img class="max-width-100 related-img" src="upload/album/thumb/<?php echo $value['avatar']; ?>"  />
                                                                        <div class="clearfix">
                                                                            <p class=" text-center"><a class="text-uppercase open-semi"><?php echo $value['title_'.$lang]; ?></a></p>
                                                                            <a href="<?php echo base_url('tin-chi-tiet/'.$value['tag']); ?>">
                                                                                <button class="read-more-ct"><?php echo $this->lang->line('xemchitiet'); ?></button>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                                    
                                                    
                                                </ul>
                                                <div class="custom-navigation-3 clearfix">
                                                    <div class="custom-controls-container-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>

    // applying photobox on a `gallery` element which has lots of thumbnails links.
      // Passing options object as well:
      //-----------------------------------------------
      $('#gallery_temp').photobox('a',{ time:0 });

      // using a callback and a fancier selector
      //----------------------------------------------
      $('#gallery_temp').photobox('li > a.family',{ time:0 }, callback);
      function callback(){
         console.log('image has been loaded');
      }

      // // destroy the plugin on a certain gallery:
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('destroy');

      // // re-initialize the photbox DOM (does what Document ready does)
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('prepareDOM');
  </script>