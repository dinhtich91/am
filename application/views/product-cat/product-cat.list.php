<?php
$description = isset($detail['desc']) ? $detail['desc'] : "";
?>
<section class="content">
    <div class="catlist-product">
        <div class="navigation clearfix">
            <ol class="breadcrumb">
                <li><?php echo $this->breadcrumb->output(); ?></li>
            </ol>

        </div>
        <div class="tygh-content clearfix">
            <div class="container-fluid content-grid">
                <div class="row-fluid ">
                    <div class="span16 breadcrumbs-grid">
                        <h1 class="mainbox-title"><span><?php echo $title_header; ?></span> </h1>
                        <div class="catlist-product-description">
                            <?php echo $description ?>
                        </div>
                    </div>
                </div>
                <div class="row-fluid ">
                    <div class="span16 main-content-grid">
                        <div id="category_products_2">
                            <div class="ty-pagination-container cm-pagination-container" id="pagination_contents">                                
                                <div class="grid-list">
                                    <?php
                                    foreach ($list as $product) {
                                        $title = $product['title'];
                                        $gia_ban = $product['sale'] > 0 ? number_format($product['sale']) . " đ" : 0;
                                        $gia_goc = $product['price'] > 0 ? number_format($product['price']) . " đ" : "Liên hệ";
                                        $tag = $product['tag'];
                                        
                                        $link = site_url($product['tag']."/chi-tiet/".$product['id']);
                                        
                                        $avatar = $product['avatar'];
                                        if ($gia_ban == 0 && $gia_goc == 0) {
                                            $sale_percent = 0;
                                        } else {
                                            $sale_percent = 100 - (int) ($gia_ban / $gia_goc * 100);
                                        }
                                        ?>
                                        <div class="ty-column5">
                                            <div class="ty-grid-list__item">
                                                <div class="ty-discount-label"> <i class="ty-icon-down-micro icon-label"></i><span class="ty-discount-label__value"><i style="font-style:normal;">GIẢM <?php echo $sale_percent ?>%</i></span></div>
                                                <div class="ty-grid-list__image">
                                                    <a href="<?php echo $link; ?>" title="<?php echo $title; ?>"> <img class="ty-pict" src="<?php echo $avatar; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" height="190" width="220"></a>
                                                </div>
                                                <div class="ty-grid-list__item-name"><a href="<?php echo $link; ?>" class="product-title" title="<?php echo $title; ?>"><?php echo $title; ?></a>
                                                </div>
                                                <div class="ty-grid-list__price "><span><span class="ty-list-price ty-nowrap"><span class="list-price-label">Giá gốc:</span> <span class="ty-strike"><span id="sec_list_price_32412" class="ty-list-price ty-nowrap"><?php echo $gia_goc ?></span></span>
                                                        </span>
                                                    </span> <span class="cm-reload-32412 ty-price-update" id="price_update_32412"><span class="ty-price" id="line_discounted_price_32412"> <strong class="ty-price-num">Giá bán: </strong> <span id="sec_discounted_price_32412" class="ty-price-num"><?php echo $gia_ban; ?></span></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="page clearfix">
                                        <ul class="pagination">
                                            <?php echo $this->pagination->create_links(); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".tygh-header").removeClass("fixed");
        $(window).scroll(function () {
            $(".tygh-header").addClass("fixed");
            if ($(window).scrollTop() == 0) {
                $(".tygh-header").removeClass("fixed");
            }
        })

    })
</script>
